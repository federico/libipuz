/* ipuz-acrostic.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-crossword.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_ACROSTIC (ipuz_acrostic_get_type ())
G_DECLARE_DERIVABLE_TYPE   (IPuzAcrostic, ipuz_acrostic, IPUZ, ACROSTIC, IPuzCrossword);


typedef struct _IPuzAcrosticClass
{
  IPuzCrosswordClass parent_class;
} IPuzAcrosticClass;


IPuzPuzzle *ipuz_acrostic_new              (void);
IPuzClue   *ipuz_acrostic_get_quote_clue   (IPuzAcrostic *self);

G_END_DECLS

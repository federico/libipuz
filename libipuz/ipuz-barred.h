/* ipuz-barred.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-crossword.h>
#include <libipuz/ipuz-style.h>
#include <libipuz/ipuz-symmetry.h>

G_BEGIN_DECLS


#define IPUZ_BARRED_STYLE_T  "T"
#define IPUZ_BARRED_STYLE_L  "L"
#define IPUZ_BARRED_STYLE_TL "TL"


#define IPUZ_TYPE_BARRED (ipuz_barred_get_type ())
G_DECLARE_DERIVABLE_TYPE  (IPuzBarred, ipuz_barred, IPUZ, BARRED, IPuzCrossword);


typedef struct _IPuzBarredClass
{
  IPuzCrosswordClass parent_class;
} IPuzBarredClass;


IPuzPuzzle     *ipuz_barred_new                   (void);
IPuzStyleSides  ipuz_barred_calculate_side_toggle (IPuzBarred     *self,
                                                   IPuzCellCoord   coord,
                                                   IPuzStyleSides  side,
                                                   IPuzSymmetry    symmetry);
IPuzStyleSides  ipuz_barred_get_cell_bars         (IPuzBarred     *self,
                                                   IPuzCellCoord   coord);
gboolean        ipuz_barred_set_cell_bars         (IPuzBarred     *self,
                                                   IPuzCellCoord   coord,
                                                   IPuzStyleSides  sides);


G_END_DECLS

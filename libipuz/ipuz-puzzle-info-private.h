/* ipuz-puzzle-info-private.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-charset.h>
#include <libipuz/ipuz-puzzle-info.h>

G_BEGIN_DECLS


struct _IPuzPuzzleInfo
{
  GObject parent_instance;

  IPuzPuzzleFlags flags;
  IPuzCellStats cell_stats;
  IPuzCharset *charset;

  /* used by crosswords */
  IPuzCharset *solution_chars;
  IPuzCharset *clue_lengths;
  guint pangram_count;
  GArray *unche_list; /* Unimplemented */
};


G_END_DECLS

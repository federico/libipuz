#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


static void
test_acrostic_load (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }
  ipuz_crossword_print (IPUZ_CROSSWORD (puzzle));
  g_assert (puzzle != NULL);
}

static void
test_acrostic_load2 (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic2.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }

  g_assert (puzzle != NULL);
}

static void
test_acrostic_quote_clue (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  IPuzClue *clue_a;
  IPuzClue *clue_b;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  /* Load file with quote clue */
  path = g_test_build_filename (G_TEST_DIST, "acrostic2.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  /* same file without quote clue */
  path = g_test_build_filename (G_TEST_DIST, "acrostic2-no-quote-clue.ipuz", NULL);
  puzzle_b = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  clue_a = ipuz_acrostic_get_quote_clue (IPUZ_ACROSTIC (puzzle_a));
  clue_b = ipuz_acrostic_get_quote_clue (IPUZ_ACROSTIC (puzzle_b));

  /* These clues will have different values of clue_set, so we can't
   * just compare that they're equal. We do want to show that they
   * have the same cells, though */
  g_assert (clue_a != NULL);
  g_assert (clue_b != NULL);
  g_assert (clue_a->cells != NULL);
  g_assert (clue_b->cells != NULL);
  g_assert (clue_a->cells->len == clue_b->cells->len);

  g_assert (memcmp (clue_a->cells->data,
                    clue_b->cells->data,
                    clue_a->cells->len * sizeof (IPuzCellCoord)) == 0);
}

static void
test_acrostic_save (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;
  g_autofree gchar *data = NULL;
  gsize length;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  data = ipuz_puzzle_save_to_data (puzzle_a, &length);
  g_print ("%s", data);
  puzzle_b = ipuz_puzzle_new_from_data (data, length, &error);
  g_assert (error == NULL);

  g_assert (ipuz_puzzle_equal (puzzle_a, puzzle_b));
}

static void
test_acrostic_deep_copy (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  puzzle_b = ipuz_puzzle_deep_copy (puzzle_a);

  g_assert (ipuz_puzzle_equal (puzzle_a, puzzle_b));
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/acrostic/load", test_acrostic_load);
  g_test_add_func ("/acrostic/load2", test_acrostic_load2);
  g_test_add_func ("/acrostic/quote_clue", test_acrostic_quote_clue);
  g_test_add_func ("/acrostic/save", test_acrostic_save);
  g_test_add_func ("/acrostic/deep_copy", test_acrostic_deep_copy);

  return g_test_run ();
}


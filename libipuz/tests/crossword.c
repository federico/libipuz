#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


static void
test_crossword_load (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "first-crossword.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }
  g_assert (puzzle != NULL);

  ipuz_crossword_print (IPUZ_CROSSWORD (puzzle));
}

static void
test_crossword_board_equal (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "first-crossword.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  puzzle_b = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  g_free (path);

  g_assert (ipuz_board_equal (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_a)),
                              ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_b))));
}

static void
test_crossword_board_different (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "4962.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  /* This is the same file, with one cell field modified... */

  path = g_test_build_filename (G_TEST_DIST, "4962-modified.ipuz", NULL);
  puzzle_b = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  /* ... so it should compare as being different. */

  g_assert (!ipuz_board_equal (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_a)),
                               ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_b))));
}

static void
test_crossword_deep_copy (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "first-crossword.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  puzzle_b = ipuz_puzzle_deep_copy (puzzle_a);

  g_free (path);

  g_assert (ipuz_board_equal (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_a)),
                              ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_b))));
}

static void
test_crossword_guesses (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  g_autofree gchar *path = NULL;
  g_autofree gchar *saved_path = NULL;
  g_autoptr(IPuzGuesses) guesses = NULL;
  g_autoptr(IPuzGuesses) saved_guesses = NULL;
  IPuzCellCoord coord;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "simple.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }
  g_assert (puzzle != NULL);

  guesses = ipuz_guesses_new_from_board (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle)),
                                         FALSE);
  g_assert (guesses);

  saved_path = g_test_build_filename (G_TEST_DIST, "simple-saved.ipuz", NULL);
  saved_guesses = ipuz_guesses_new_from_file (saved_path, &error);
  g_assert (saved_guesses);

  /* These should be different to start */
  g_assert (!ipuz_guesses_equal (saved_guesses, guesses));

  coord.row = 0; coord.column = 0;
  ipuz_guesses_set_guess (guesses, coord, "C");

  coord.row = 2; coord.column = 1;
  ipuz_guesses_set_guess (guesses, coord, "Q");

  coord.row = 2; coord.column = 2;
  ipuz_guesses_set_guess (guesses, coord, "O");

  /* Now they should be the same */
  g_assert (ipuz_guesses_equal (saved_guesses, guesses));

  /* FIXME: Figure out where I'm supposed to write files during tests */
  //  g_assert (ipuz_guesses_save_to_file (saved_guesses, "/tmp/test.json", &error));
}


static void
test_crossword_calc_clues (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  /* Load a file with no clue numbers */
  path = g_test_build_filename (G_TEST_DIST, "calc.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);


  /* same puzzle but with the correct clue nubmers */
  path = g_test_build_filename (G_TEST_DIST, "calc-corrected.ipuz", NULL);
  puzzle_b = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  /* it should compare as being different. */
  g_assert (!ipuz_board_equal (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_a)),
                               ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_b))));

  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle_a), NULL);

  /* And now they should compare as being the same. */
  g_assert (ipuz_board_equal (ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_a)),
                              ipuz_crossword_get_board (IPUZ_CROSSWORD (puzzle_b))));
}

static IPuzStyle *
get_style (IPuzPuzzle *puzzle, IPuzCellCoord coord)
{
  IPuzCell *cell = ipuz_crossword_get_cell (IPUZ_CROSSWORD (puzzle), coord);
  g_assert (cell != NULL);
  return ipuz_cell_get_style (cell);
}

static IPuzStyleShape
get_shapebg (IPuzPuzzle *puzzle, IPuzCellCoord coord)
{
  IPuzStyle *style = get_style (puzzle, coord);
  g_assert (style != NULL);
  return ipuz_style_get_shapebg (style);
}

static IPuzStyleSides
get_barred (IPuzPuzzle *puzzle, IPuzCellCoord coord)
{
  IPuzStyle *style = get_style (puzzle, coord);
  g_assert (style != NULL);
  return ipuz_style_get_barred (style);
}

static void
test_crossword_load_styles (void)
{
  g_autoptr (IPuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  /* this file has styles in it */
  path = g_test_build_filename (G_TEST_DIST, "cloud.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_assert_no_error (error);

  IPuzCellCoord coord = {
    .row = 1,
    .column = 9,
  };
  g_assert_cmpint (get_shapebg (puzzle, coord), ==, IPUZ_STYLE_SHAPE_CIRCLE);

  // Test backslash to see if quoting is done correctly.

  // test style by reference
  coord.column = 10;
  g_assert_cmpint (get_shapebg (puzzle, coord), ==, IPUZ_STYLE_SHAPE_BACKSLASH);

  // test style inline
  coord.column = 11;
  g_assert_cmpint (get_shapebg (puzzle, coord), ==, IPUZ_STYLE_SHAPE_BACKSLASH);

  // style with no shapebg
  coord.column = 12;
  g_assert_cmpint (get_shapebg (puzzle, coord), ==, IPUZ_STYLE_SHAPE_NONE);

  // no style
  coord.row = 2;
  coord.column = 7;
  g_assert_null (get_style (puzzle, coord));

  // unsupported shapebg, falls back to NONE
  coord.column = 8;
  g_assert_cmpint (get_shapebg (puzzle, coord), ==, IPUZ_STYLE_SHAPE_NONE);

  // barred style
  coord.row = 15;
  coord.column = 0;
  g_assert_cmpint (get_barred (puzzle, coord), ==, IPUZ_STYLE_SIDES_TOP | IPUZ_STYLE_SIDES_RIGHT);

  coord.row = 15;
  coord.column = 1;
  g_assert_cmpint (get_barred (puzzle, coord), ==, IPUZ_STYLE_SIDES_BOTTOM | IPUZ_STYLE_SIDES_LEFT);

  g_free (path);
}


static void
test_crossword_save (void)
{
  g_autoptr (IPuzPuzzle) puzzle_a = NULL;
  g_autoptr (IPuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;
  g_autofree gchar *data = NULL;
  gsize length;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "first-crossword.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  data = ipuz_puzzle_save_to_data (puzzle_a, &length);
  g_print ("%s", data);
  puzzle_b = ipuz_puzzle_new_from_data (data, length, &error);
  g_assert (error == NULL);

  g_assert (ipuz_puzzle_equal (puzzle_a, puzzle_b));
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/crossword/load", test_crossword_load);
  g_test_add_func ("/crossword/board_equal", test_crossword_board_equal);
  g_test_add_func ("/crossword/board_different", test_crossword_board_different);
  g_test_add_func ("/crossword/deep_copy", test_crossword_deep_copy);
  g_test_add_func ("/crossword/guesses", test_crossword_guesses);
  g_test_add_func ("/crossword/calc_clues", test_crossword_calc_clues);
  g_test_add_func ("/crossword/load_styles", test_crossword_load_styles);
  g_test_add_func ("/crossword/save", test_crossword_save);

  return g_test_run ();
}


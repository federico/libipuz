/* ipuz-clue.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-enumeration.h>

G_BEGIN_DECLS


typedef enum _IPuzClueDirection
{
  IPUZ_CLUE_DIRECTION_NONE,               /* @ No direction. Used for errors, and to indicate unset IPuzClueId @ */
  IPUZ_CLUE_DIRECTION_ACROSS,             /* @ Across clues @ */
  IPUZ_CLUE_DIRECTION_DOWN,               /* @ Down clues @ */
  IPUZ_CLUE_DIRECTION_DIAGONAL,           /* @ Diagonal clues which go down and to the right @ */
  IPUZ_CLUE_DIRECTION_DIAGONAL_UP,        /* @ Diagonal clues which go up and to the right @ */
  IPUZ_CLUE_DIRECTION_DIAGONAL_DOWN_LEFT, /* @ Diagonal clues which go down and to the left @ */
  IPUZ_CLUE_DIRECTION_DIAGONAL_UP_LEFT,   /* @ Diagonal clues which go up and to the left @ */
  IPUZ_CLUE_DIRECTION_ZONES,              /* @ Clues for explicitly-specified zones @ */
  IPUZ_CLUE_DIRECTION_CLUES,              /* @ Pre-ordered clues (usually alphabetical) @ */
  IPUZ_CLUE_DIRECTION_HIDDEN,             /* @ Clues that aren't shown to the user @ */
  IPUZ_CLUE_DIRECTION_CUSTOM,             /* @ Start of the range of custom directions  @ */
  /* ... */
} IPuzClueDirection;


#define IPUZ_CLUE_DIRECTION_HEADING(dir) (((dir)>=IPUZ_CLUE_DIRECTION_ACROSS)&&((dir)<=IPUZ_CLUE_DIRECTION_DIAGONAL_UP_LEFT))


/* IPuzClueId */

/* Unique id for a clue.
 *
 * Within a puzzle, clues are identified by a pair of:
 * (direction, index)
 * eg.  "the 5th index for the Down clues".
 *
 * If direction == IPUZ_CLUE_DIRECTION_NONE, then the number is
 * meaningless, and this means that there is no clue to be identified
 * (e.g. instead of a NULL pointer for clue). Note, IPuzClueIds aren't
 * stable when editing puzzles.
 */
typedef struct
{
  IPuzClueDirection direction;
  guint index;
} IPuzClueId;

GType       ipuz_clue_id_get_type (void) G_GNUC_CONST;
IPuzClueId *ipuz_clue_id_copy     (const IPuzClueId *clue_id);
void        ipuz_clue_id_free     (IPuzClueId       *clue_id);
gboolean    ipuz_clue_id_equal    (const IPuzClueId *a,
                                   const IPuzClueId *b);

#define IPUZ_CLUE_ID_IS_UNSET(c) (c.direction==IPUZ_CLUE_DIRECTION_NONE)
#define IPUZ_TYPE_CLUE_ID (ipuz_clue_id_get_type())


/* IPuz Cell Coord */


typedef struct
{
  guint row;
  guint column;
} IPuzCellCoord;

GType            ipuz_cell_coord_get_type (void) G_GNUC_CONST;
IPuzCellCoord   *ipuz_cell_coord_copy     (const IPuzCellCoord *coord);
gboolean         ipuz_cell_coord_equal    (const IPuzCellCoord *a,
                                           const IPuzCellCoord *b);

#define IPUZ_TYPE_CELL_COORD (ipuz_cell_coord_get_type())


/* IPuz Clue */
typedef struct _IPuzClue IPuzClue;
struct _IPuzClue
{
  /* Just fields for crosswords, to start. */
  gint number;
  gchar *label;
  gchar *clue_text;
  IPuzClueDirection direction;
  GArray *cells; /* IPuzCellCoord */

  /* < Private > */
  IPuzEnumeration *enumeration;
  IPuzCellCoord location; /* Used for arrowwords */
  gboolean cells_set;     /* TRUE, if cells were loaded vs. calculated */
  gboolean location_set;
};

#define IPUZ_TYPE_CLUE (ipuz_clue_get_type ())


GType              ipuz_clue_get_type              (void);
IPuzClue          *ipuz_clue_new                   (void);
IPuzClue          *ipuz_clue_copy                  (const IPuzClue    *clue);
void               ipuz_clue_free                  (IPuzClue          *clue);
gboolean           ipuz_clue_equal                 (const IPuzClue    *clue1,
                                                    const IPuzClue    *clue2);
void               ipuz_clue_build                 (IPuzClue          *clue,
                                                    JsonBuilder       *builder);
IPuzClueDirection  ipuz_clue_direction_switch      (IPuzClueDirection  direction);
gint               ipuz_clue_get_number            (IPuzClue          *clue);
void               ipuz_clue_set_number            (IPuzClue          *clue,
                                                    gint               number);
const gchar       *ipuz_clue_get_label             (IPuzClue          *clue);
void               ipuz_clue_set_label             (IPuzClue          *clue,
                                                    const gchar       *label);
const gchar       *ipuz_clue_get_clue_text         (IPuzClue          *clue);
void               ipuz_clue_set_clue_text         (IPuzClue          *clue,
                                                    const gchar       *clue_text);
IPuzEnumeration   *ipuz_clue_get_enumeration       (IPuzClue          *clue);
void               ipuz_clue_set_enumeration       (IPuzClue          *clue,
                                                    IPuzEnumeration   *enumeration);
IPuzClueDirection  ipuz_clue_get_direction         (IPuzClue          *clue);
void               ipuz_clue_set_direction         (IPuzClue          *clue,
                                                    IPuzClueDirection  direction);
IPuzCellCoord      ipuz_clue_get_location          (IPuzClue          *clue,
                                                    gboolean          *location_set);
void               ipuz_clue_set_location          (IPuzClue          *clue,
                                                    IPuzCellCoord      location);
const GArray      *ipuz_clue_get_cells             (IPuzClue          *clue);
void               ipuz_clue_append_cell           (IPuzClue          *clue,
                                                    IPuzCellCoord      coord);
void               ipuz_clue_get_first_cell        (IPuzClue          *clue,
                                                    IPuzCellCoord     *coord);
void               ipuz_clue_get_last_cell         (IPuzClue          *clue,
                                                    IPuzCellCoord     *coord);
gboolean           ipuz_clue_contains_cell         (IPuzClue          *clue,
                                                    IPuzCellCoord      coord);
IPuzClue          *ipuz_clue_new_from_json         (JsonNode          *node);
void               ipuz_clue_ensure_enumeration    (IPuzClue          *clue);


/* Helper functions */
const gchar       *ipuz_clue_direction_to_string   (IPuzClueDirection  direction);
IPuzClueDirection  ipuz_clue_direction_from_string (const gchar       *str);


G_END_DECLS

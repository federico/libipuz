/* ipuz-crossword.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include "libipuz.h"
#include "libipuz-enums.h"
#include "ipuz-clue-sets.h"
#include "ipuz-magic.h"
#include "ipuz-puzzle-info-private.h"
#include "rust/ipuz-rust.h"

#include <libipuz/ipuz-puzzle.h>
#include <libipuz/ipuz-crossword.h>
#include <libipuz/ipuz-board.h>
#include <glib/gi18n-lib.h>


enum
{
  PROP_0,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_SHOWENUMERATIONS,
  PROP_CLUE_PLACEMENT,
  PROP_BOARD,
  PROP_GUESSES,
  N_PROPS
};
static GParamSpec *obj_props[N_PROPS] = { NULL, };


typedef struct _IPuzCrosswordPrivate
{
  gint width;
  gint height;
  gboolean showenumerations;
  IPuzClueSets *clue_sets;

  IPuzBoard *board;
  IPuzGuesses *guesses;

  gboolean uses_extensions;
  gboolean has_solution;
  gboolean has_saved;
  IPuzCluePlacement clue_placement;
  /*
"zones": [ GroupSpec, ... ]
		Arbitrarily-shaped entries overlaid on the grid
"answer": "entry"
		The final answer to the puzzle
"answers": [ "entry", ... ]
		List of final answers to the puzzle
"enumeration": Enumeration
		Enumeration of the final answer to the puzzle
"enumerations": [ Enumeration, ... ]
		List of enumerations for final answers to the puzzle
"misses": { "entry": "hint", ... }
		List of hints to be given for misses
  */
} IPuzCrosswordPrivate;


static void                ipuz_crossword_init                      (IPuzCrossword      *self);
static void                ipuz_crossword_class_init                (IPuzCrosswordClass *klass);
static void                ipuz_crossword_finalize                  (GObject            *object);
static void                ipuz_crossword_set_property              (GObject            *object,
                                                                     guint               prop_id,
                                                                     const GValue       *value,
                                                                     GParamSpec         *pspec);
static void                ipuz_crossword_get_property              (GObject            *object,
                                                                     guint               prop_id,
                                                                     GValue             *value,
                                                                     GParamSpec         *pspec);
static void                ipuz_crossword_set_property              (GObject            *object,
                                                                     guint               prop_id,
                                                                     const GValue       *value,
                                                                     GParamSpec         *pspec);
static void                ipuz_crossword_get_property              (GObject            *object,
                                                                     guint               prop_id,
                                                                     GValue             *value,
                                                                     GParamSpec         *pspec);
static void                ipuz_crossword_load_node                 (IPuzPuzzle         *puzzle,
                                                                     const char         *member_name,
                                                                     JsonNode           *node);
static void                ipuz_crossword_post_load_node            (IPuzPuzzle         *puzzle,
                                                                     const char         *member_name,
                                                                     JsonNode           *node);
static void                ipuz_crossword_fixup                     (IPuzPuzzle         *puzzle);
static void                ipuz_crossword_build                     (IPuzPuzzle         *puzzle,
                                                                     JsonBuilder        *builder);
static IPuzPuzzleFlags     ipuz_crossword_get_flags                 (IPuzPuzzle         *puzzle);
static void                ipuz_crossword_calculate_info            (IPuzPuzzle         *puzzle,
                                                                     IPuzPuzzleInfo      *info);
static void                ipuz_crossword_clone                     (IPuzPuzzle         *src,
                                                                     IPuzPuzzle         *dest);
static const gchar *const *ipuz_crossword_get_kind_str              (IPuzPuzzle         *puzzle);
static void                ipuz_crossword_set_style                 (IPuzPuzzle         *puzzle,
                                                                     const gchar        *style_name,
                                                                     IPuzStyle          *style);
static gboolean            ipuz_crossword_equal                     (IPuzPuzzle         *puzzle_a,
                                                                     IPuzPuzzle         *puzzle_b);
static void                ipuz_crossword_real_fix_symmetry         (IPuzCrossword      *self,
                                                                     IPuzSymmetry        symmetry,
                                                                     GArray             *coords);
static void                ipuz_crossword_real_fix_numbering        (IPuzCrossword      *self);
static void                ipuz_crossword_real_fix_clues            (IPuzCrossword      *self);
static void                ipuz_crossword_real_fix_enumerations     (IPuzCrossword      *self);
static void                ipuz_crossword_real_fix_styles           (IPuzCrossword      *self);
static void                ipuz_crossword_real_fix_all              (IPuzCrossword      *self,
                                                                     const gchar        *first_attribute_name,
                                                                     va_list             var_args);
static gboolean            ipuz_crossword_real_clue_continues_up    (IPuzCrossword      *self,
                                                                     IPuzCellCoord       coord);
static gboolean            ipuz_crossword_real_clue_continues_down  (IPuzCrossword      *self,
                                                                     IPuzCellCoord       coord);
static gboolean            ipuz_crossword_real_clue_continues_left  (IPuzCrossword      *self,
                                                                     IPuzCellCoord       coord);
static gboolean            ipuz_crossword_real_clue_continues_right (IPuzCrossword      *self,
                                                                     IPuzCellCoord       coord);
static void                ipuz_crossword_real_mirror_cell          (IPuzCrossword      *self,
                                                                     IPuzCellCoord       src_coord,
                                                                     IPuzCellCoord       dest_coord,
                                                                     IPuzSymmetry        symmetry,
                                                                     IPuzSymmetryOffset  symmetry_offset);
static gboolean            ipuz_crossword_real_check_mirror         (IPuzCrossword      *self,
                                                                     IPuzCellCoord       src_coord,
                                                                     IPuzCellCoord       target_coord,
                                                                     IPuzSymmetry        symmetry,
                                                                     IPuzSymmetryOffset  symmetry_offset);
static gboolean            ipuz_crossword_real_set_size             (IPuzCrossword      *self,
                                                                     gint                width,
                                                                     gint                height);
static IPuzClue           *calculate_clue                           (IPuzCrossword      *self,
                                                                     IPuzClueDirection   direction,
                                                                     IPuzCellCoord       coord,
                                                                     gint                number);


G_DEFINE_TYPE_WITH_CODE (IPuzCrossword, ipuz_crossword, IPUZ_TYPE_PUZZLE,
                         G_ADD_PRIVATE (IPuzCrossword));


static void
ipuz_crossword_init_clues (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  g_clear_pointer (&priv->clue_sets, ipuz_clue_sets_unref);
  priv->clue_sets = ipuz_clue_sets_new ();
}

static void
ipuz_crossword_init (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (self);

  priv->showenumerations = FALSE;
  priv->width = 0;
  priv->height = 0;
  priv->board = ipuz_board_new ();
  ipuz_crossword_init_clues (self);

  ipuz_noop ();
}

static void
ipuz_crossword_class_init (IPuzCrosswordClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);
  IPuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);
  IPuzCrosswordClass *crossword_class = IPUZ_CROSSWORD_CLASS (klass);

  object_class->finalize = ipuz_crossword_finalize;
  object_class->set_property = ipuz_crossword_set_property;
  object_class->get_property = ipuz_crossword_get_property;
  puzzle_class->load_node = ipuz_crossword_load_node;
  puzzle_class->post_load_node = ipuz_crossword_post_load_node;
  puzzle_class->fixup = ipuz_crossword_fixup;
  puzzle_class->build = ipuz_crossword_build;
  puzzle_class->get_flags = ipuz_crossword_get_flags;
  puzzle_class->calculate_info = ipuz_crossword_calculate_info;
  puzzle_class->clone = ipuz_crossword_clone;
  puzzle_class->get_kind_str = ipuz_crossword_get_kind_str;
  puzzle_class->set_style = ipuz_crossword_set_style;
  puzzle_class->equal = ipuz_crossword_equal;
  crossword_class->fix_symmetry = ipuz_crossword_real_fix_symmetry;
  crossword_class->fix_numbering = ipuz_crossword_real_fix_numbering;
  crossword_class->fix_clues = ipuz_crossword_real_fix_clues;
  crossword_class->fix_enumerations = ipuz_crossword_real_fix_enumerations;
  crossword_class->fix_styles = ipuz_crossword_real_fix_styles;
  crossword_class->fix_all = ipuz_crossword_real_fix_all;
  crossword_class->clue_continues_up = ipuz_crossword_real_clue_continues_up;
  crossword_class->clue_continues_down = ipuz_crossword_real_clue_continues_down;
  crossword_class->clue_continues_left = ipuz_crossword_real_clue_continues_left;
  crossword_class->clue_continues_right = ipuz_crossword_real_clue_continues_right;
  crossword_class->mirror_cell = ipuz_crossword_real_mirror_cell;
  crossword_class->check_mirror = ipuz_crossword_real_check_mirror;

  obj_props[PROP_WIDTH] = g_param_spec_int ("width",
                                            _("width"),
                                            _("Width of the puzzle grid"),
                                            0, 65536, 0,
                                            G_PARAM_READWRITE);
  obj_props[PROP_HEIGHT] = g_param_spec_int ("height",
                                             _("height"),
                                             _("height of the puzzle grid"),
                                             0, 65536, 0,
                                             G_PARAM_READWRITE);
  obj_props[PROP_SHOWENUMERATIONS] = g_param_spec_boolean ("showenumerations",
                                                           _("Show Enumerations"),
                                                           _("Show enumerations with clues"),
                                                           FALSE,
                                                           G_PARAM_READWRITE);
  obj_props[PROP_CLUE_PLACEMENT] = g_param_spec_enum ("clue-placement",
                                                      _("Clue Placement"),
                                                      _("Where to put clues"),
                                                      I_TYPE_PUZ_CLUE_PLACEMENT,
                                                      IPUZ_CLUE_PLACEMENT_NULL,
                                                      G_PARAM_READWRITE);
  obj_props[PROP_BOARD] = g_param_spec_object ("board",
                                               _("Board"),
                                               _("The crossword board"),
                                               IPUZ_TYPE_BOARD,
                                               G_PARAM_READABLE);
  obj_props[PROP_GUESSES] = g_param_spec_boxed ("guesses",
                                                _("Guesses"),
                                                _("The guesses associated with this crossword"),
                                                IPUZ_TYPE_GUESSES,
                                                G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

/* free all memory */
static void
ipuz_crossword_finalize (GObject *object)
{
  IPuzCrosswordPrivate *priv;

  g_return_if_fail (object != NULL);

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (object));

  g_clear_pointer (&priv->clue_sets, ipuz_clue_sets_unref);

  g_object_unref (G_OBJECT (priv->board));
  ipuz_guesses_unref (priv->guesses);

  G_OBJECT_CLASS (ipuz_crossword_parent_class)->finalize (object);
}

static void
ipuz_crossword_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (object));

  switch (prop_id)
    {
    case PROP_WIDTH:
      ipuz_crossword_real_set_size (IPUZ_CROSSWORD (object),
                                    g_value_get_int (value),
                                    priv->height);
      break;
    case PROP_HEIGHT:
      ipuz_crossword_real_set_size (IPUZ_CROSSWORD (object),
                                    priv->width,
                                    g_value_get_int (value));
      break;
    case PROP_SHOWENUMERATIONS:
      priv->showenumerations = g_value_get_boolean (value);
      break;
    case PROP_CLUE_PLACEMENT:
      priv->clue_placement = g_value_get_enum (value);
      break;
    case PROP_GUESSES:
      ipuz_crossword_set_guesses (IPUZ_CROSSWORD (object), (IPuzGuesses *)g_value_get_boxed (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_crossword_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (object));

  switch (prop_id)
    {
    case PROP_WIDTH:
      g_value_set_int (value, priv->width);
      break;
    case PROP_HEIGHT:
      g_value_set_int (value, priv->height);
      break;
    case PROP_SHOWENUMERATIONS:
      g_value_set_boolean (value, priv->showenumerations);
      break;
    case PROP_CLUE_PLACEMENT:
      g_value_set_enum (value, priv->clue_placement);
      break;
    case PROP_BOARD:
      g_value_set_object (value, priv->board);
      break;
    case PROP_GUESSES:
      g_value_set_boxed (value, priv->guesses);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
load_dimensions (IPuzCrossword *self,
                 JsonNode      *node)
{
  gint width = -1;
  gint height = -1;

  if (!JSON_NODE_HOLDS_OBJECT (node))
    return;

  g_autoptr(JsonReader) reader = json_reader_new (node);
  if (json_reader_read_member (reader, "width"))
    width = json_reader_get_int_value (reader);
  json_reader_end_member (reader);

  if (json_reader_read_member (reader, "height"))
    height = json_reader_get_int_value (reader);
  json_reader_end_member (reader);

  if (width > 0 && height > 0)
    ipuz_crossword_real_set_size (self, width, height);
}

static void
load_clues_foreach (JsonArray *array,
                    guint      index,
                    JsonNode  *element_node,
                    gpointer   user_data)
{
  IPuzClue *clue;

  clue = ipuz_clue_new_from_json (element_node);
  GArray *clues = (GArray *) user_data;
  g_array_append_val (clues, clue);
}

static void
load_clues (IPuzCrossword *self,
            JsonNode      *node)
{
  IPuzCrosswordPrivate *priv;
  JsonObjectIter iter = {0, };
  const gchar *member_name = NULL;
  JsonNode *member_node;

  if (!JSON_NODE_HOLDS_OBJECT (node))
    return;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  json_object_iter_init (&iter, json_node_get_object (node));
  while (json_object_iter_next (&iter, &member_name, &member_node))
    {
      IPuzClueDirection direction;
      GArray *clues;
      gchar **strv;

      /* conditions for parsing clues. It has to be a direction we know, and has
       * to be an array. We should look into better error handling here.
       * FIXME(error-handling): See issue #26
       */
      if (! JSON_NODE_HOLDS_ARRAY (member_node))
        continue;

      strv = g_strsplit_set (member_name, ":", 2);
      if (strv == NULL || strv[0] == NULL)
        continue;

      direction = ipuz_clue_sets_add_set (priv->clue_sets,
                                          ipuz_clue_direction_from_string (strv[0]),
                                          strv[1]);
      clues = ipuz_clue_sets_get_clues (priv->clue_sets, direction);
      g_strfreev (strv);

      if (direction == IPUZ_CLUE_DIRECTION_NONE)
        continue;

      /* Load the clues */
      json_array_foreach_element (json_node_get_array (member_node), load_clues_foreach, clues);

      /* The json section of each clue doesn't contain the direction. Go back and set it
       * as a second pass.
       */
      for (guint i = 0; i < clues->len; i++)
        ipuz_clue_set_direction (g_array_index (clues, IPuzClue *, i), direction);
    }
}

static void
load_clue_placement (IPuzCrossword *self,
                     JsonNode      *node)
{
  IPuzCrosswordPrivate *priv;
  const gchar *clueplacement = NULL;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  clueplacement = json_node_get_string (node);
  if (clueplacement == NULL)
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_NULL;
  else if (g_strcmp0 (clueplacement, "before") == 0)
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_BEFORE;
  else if (g_strcmp0 (clueplacement, "after") == 0)
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_AFTER;
  else if (g_strcmp0 (clueplacement, "blocks") == 0)
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_BLOCKS;
  else
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_NULL;
}

static void
ipuz_crossword_load_node (IPuzPuzzle *puzzle,
                          const char *member_name,
                          JsonNode   *node)
{

  g_return_if_fail (member_name != NULL);
  g_return_if_fail (node != NULL);

  if (strcmp (member_name, "dimensions") == 0)
    {
      load_dimensions (IPUZ_CROSSWORD (puzzle), node);
      return;
    }
  if (strcmp (member_name, "clues") == 0)
    {
      load_clues (IPUZ_CROSSWORD (puzzle), node);
      return;
    }
  if (strcmp (member_name, "clueplacement") == 0)
    {
      load_clue_placement (IPUZ_CROSSWORD (puzzle), node);
      return;
    }

  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->load_node (puzzle, member_name, node);
}


static void
ipuz_crossword_post_load_node (IPuzPuzzle *puzzle,
                               const char *member_name,
                               JsonNode   *node)
{
  IPuzCrosswordPrivate *priv;

  g_return_if_fail (member_name != NULL);
  g_return_if_fail (node != NULL);

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle));

  if (strcmp (member_name, "puzzle") == 0)
    {
      g_autofree gchar *block, *empty;
      g_object_get (G_OBJECT (puzzle),
                    "block", &block,
                    "empty", &empty,
                    NULL);
      ipuz_board_parse_puzzle (priv->board, node, block, empty);
      return;
    }
  if (strcmp (member_name, "solution") == 0)
    {
      g_autofree gchar *block, *charset;
      g_object_get (G_OBJECT (puzzle),
                    "block", &block,
                    "charset", &charset,
                    NULL);
      ipuz_board_parse_solution (priv->board, node, block, charset);
      priv->has_solution = TRUE;
      return;
    }
  if (strcmp (member_name, "saved") == 0)
    {
      /* FIXME(saved): Load the saved guesses from disk */
    }
  /* We do not chain up! */
}

/* This will try its best to determine the cells of the clues when
 * there aren't any provided by then puzzle. There are times we can't
 * detect anything, and clues won't have cells associated with
 * them. That happens with some puzzles (such as alphabetic) */
static void
autodetect_cells_for_clue (IPuzClue     *clue,
                           IPuzClueSets *clue_sets,
                           IPuzBoard    *board)
{
  IPuzCell *cell;
  IPuzCellCoord coord;
  const GArray *cells;
  IPuzClueDirection direction;

  cells = ipuz_clue_get_cells (clue);
  g_assert (cells->len == 0);

  cell = ipuz_board_get_cell_by_clue (board, clue, &coord);

  if (cell == NULL)
    /* We have a clue with a label/number that's not in the
     * grid.
     */
    return;

  direction = ipuz_clue_sets_get_original_direction (clue_sets,
                                                     ipuz_clue_get_direction (clue));

  if (! IPUZ_CLUE_DIRECTION_HEADING (direction))
    return;

  while (cell != NULL && ipuz_cell_get_cell_type (cell) == IPUZ_CELL_NORMAL)
    {
      ipuz_clue_append_cell (clue, coord);
      ipuz_cell_set_clue (cell, clue);

      switch (direction)
        {
        case IPUZ_CLUE_DIRECTION_ACROSS:
          coord.column ++;
          break;
        case IPUZ_CLUE_DIRECTION_DOWN:
          coord.row ++;
          break;
        case IPUZ_CLUE_DIRECTION_DIAGONAL:
          coord.column ++;
          coord.row ++;
          break;
        case IPUZ_CLUE_DIRECTION_DIAGONAL_UP:
          coord.column ++;
          coord.row --;
          break;
        case IPUZ_CLUE_DIRECTION_DIAGONAL_DOWN_LEFT:
          coord.column --;
          coord.row ++;
          break;
        case IPUZ_CLUE_DIRECTION_DIAGONAL_UP_LEFT:
          coord.column --;
          coord.row --;
          break;
        default:
          /* We don't know which way to go from here. Just include the
           * first cell */
          break;
        }
      cell = ipuz_board_get_cell (board, coord);
    }
}

/* This function makes sure that each clue has its "cells" set correctly.
 * Either its explicitly set by the puzzle, or implicitly done by the board
 * itself. Either way, we need to link back from the Cell to the Clue.
 *
 * Question: Should we move this to IPuzClue? We should also split it in half.
 * */
static void
crossword_ensure_cells (IPuzClue     *clue,
                        IPuzClueSets *clue_sets,
                        IPuzBoard    *board)
{
  const GArray *cells;
  IPuzCellCoord coord;
  IPuzCell *cell;

  cells = ipuz_clue_get_cells (clue);

  /* Try to autodetect the cells */
  if (cells->len == 0)
    autodetect_cells_for_clue (clue, clue_sets, board);

  /* the clue already has cells loaded. */
  if (cells->len > 0)
    {
      for (guint i = 0; i < cells->len; i ++)
        {
          coord = g_array_index (clue->cells, IPuzCellCoord, i);
          cell = ipuz_board_get_cell (board, coord);
          ipuz_cell_set_clue (cell, clue);
        }
      return;
    }
}

/* Makes sure cells have their style associated with them */
static void
ipuz_crossword_fixup_cells (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;
  g_autoptr(GHashTable) styles = NULL;
  guint row, col;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  g_object_get (G_OBJECT (self),
                "styles", &styles,
                NULL);

  if (styles == NULL)
    return;

  for (row = 0; row < (guint) priv->height; row++)
    {
      for (col = 0; col < (guint) priv->width; col++)
        {
          IPuzCell *cell;
          IPuzCellCoord coord = { .row = row, .column = col };

          cell = ipuz_crossword_get_cell (self, coord);
          if (cell->style_name)
            {
              IPuzStyle *style;

              /* FIXME: we shouldn't touch this directly. Instad, we
               * should add a get_style_name() function */
              style = g_hash_table_lookup (styles, cell->style_name);
              ipuz_cell_set_style (cell, style, cell->style_name);
            }

          if (IPUZ_CELL_IS_BLOCK (cell) && cell->style != NULL)
            priv->uses_extensions = TRUE;

          if (cell->saved_guess != NULL)
            priv->has_saved = TRUE;
        }
    }
}

static  void
ipuz_crossword_fixup_clues_helper (IPuzClueSets            *clue_sets,
                                   IPuzClueDirection        direction,
                                   gpointer                 user_data)
{
  IPuzCrosswordPrivate *priv;
  GArray *clues;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (user_data));
  clues = ipuz_clue_sets_get_clues (clue_sets, direction);

  for (guint i = 0; i < clues->len; i++)
    {
      IPuzClue *clue;

      clue = g_array_index (clues, IPuzClue *, i);
      crossword_ensure_cells (clue, clue_sets, priv->board);
      if (priv->showenumerations)
        ipuz_clue_ensure_enumeration (clue);
    }
}

/* Make sure every clue has its "cells" set.
 * Update the enumeration if necessary
 */
static void
ipuz_crossword_fixup_clues (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  ipuz_clue_sets_foreach (priv->clue_sets,
                          ipuz_crossword_fixup_clues_helper,
                          self);
}

static void
ipuz_crossword_fixup (IPuzPuzzle *puzzle)
{
  IPuzCrossword *self;

  self = IPUZ_CROSSWORD (puzzle);

  ipuz_crossword_fixup_clues (self);
  ipuz_crossword_fixup_cells (self);

  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->fixup (puzzle);
}

static void
build_dimensions (IPuzCrossword *self,
                  JsonBuilder   *builder)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (self);

  json_builder_set_member_name (builder, "dimensions");
  json_builder_begin_object (builder);
  json_builder_set_member_name (builder, "width");
  json_builder_add_int_value (builder, priv->width);
  json_builder_set_member_name (builder, "height");
  json_builder_add_int_value (builder, priv->height);
  json_builder_end_object (builder);
}

static void
build_clues (IPuzCrossword *self,
             JsonBuilder   *builder,
             const gchar   *member,
             GArray        *clues)
{
  guint i;

  if (clues->len == 0)
    return;
  json_builder_set_member_name (builder, member);
  json_builder_begin_array (builder);
  for (i = 0; i < clues->len; i++)
    {
      IPuzClue *clue;
      clue = g_array_index (clues, IPuzClue *, i);
      ipuz_clue_build (clue, builder);
    }
  json_builder_end_array (builder);
}

typedef struct
{
  IPuzPuzzle *puzzle;
  JsonBuilder *builder;
} BuildHelperTuple;

static void
build_helper (IPuzClueSets      *clue_sets,
              IPuzClueDirection  direction,
              gpointer           user_data)
{
  BuildHelperTuple *tuple = user_data;
  IPuzClueDirection original_direction;
  const gchar *label;
  GArray *clues;

  original_direction = ipuz_clue_sets_get_original_direction (clue_sets, direction);
  label = ipuz_clue_sets_get_label (clue_sets, direction);
  clues = ipuz_clue_sets_get_clues (clue_sets, direction);

  if (label)
    {
      g_autofree gchar *member = NULL;
      member = g_strconcat (ipuz_clue_direction_to_string (original_direction),
                            ":", label, NULL);
      build_clues (IPUZ_CROSSWORD (tuple->puzzle), tuple->builder,
                   member, clues);
    }
  else
    {
      build_clues (IPUZ_CROSSWORD (tuple->puzzle), tuple->builder,
                   ipuz_clue_direction_to_string (original_direction),
                   clues);
    }
}


static void
ipuz_crossword_build (IPuzPuzzle  *puzzle,
                      JsonBuilder *builder)
{
  IPuzCrosswordPrivate *priv;
  g_autofree gchar *block = NULL;
  g_autofree gchar *empty = NULL;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle));

  g_object_get (G_OBJECT (puzzle),
                "block", &block,
                "empty", &empty,
                NULL);

  /* We chain to the parent class first to get meta-information. Not
   * every parsing section can handle this coming at the end. */
  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->build (puzzle, builder);

  build_dimensions (IPUZ_CROSSWORD (puzzle), builder);
  json_builder_set_member_name (builder, "showenumerations");
  json_builder_add_boolean_value (builder, priv->showenumerations);
  if (priv->clue_placement != IPUZ_CLUE_PLACEMENT_NULL)
    {
      const gchar *clueplacement = NULL;

      switch (priv->clue_placement)
        {
        case IPUZ_CLUE_PLACEMENT_BEFORE:
          clueplacement = "before";
          break;
        case IPUZ_CLUE_PLACEMENT_AFTER:
          clueplacement = "after";
          break;
        case IPUZ_CLUE_PLACEMENT_BLOCKS:
          clueplacement = "blocks";
          break;
        default:
          break;
        }
      if (clueplacement)
        {
          json_builder_set_member_name (builder, "clueplacement");
          json_builder_add_string_value (builder, clueplacement);
        }
    }

  ipuz_board_build_puzzle (priv->board, builder, block, empty);
  ipuz_board_build_solution (priv->board, builder, block);


  if (ipuz_clue_sets_get_n_clue_sets (priv->clue_sets) > 0)
    {
      BuildHelperTuple tuple;
      tuple.builder = builder;
      tuple.puzzle = puzzle;

      json_builder_set_member_name (builder, "clues");
      json_builder_begin_object (builder);

      ipuz_clue_sets_foreach (priv->clue_sets, build_helper, &tuple);

      json_builder_end_object (builder);
    }
}

static IPuzPuzzleFlags
ipuz_crossword_get_flags (IPuzPuzzle *puzzle)
{
  IPuzCrosswordPrivate *priv;
  guint flags;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle));

  flags = IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->get_flags (puzzle);

  /* Do we have any clues at all? */
  for (guint i = 0; i < ipuz_clue_sets_get_n_clue_sets (priv->clue_sets); i++)
    {
      GArray *clues;

      clues = ipuz_clue_sets_get_clues (priv->clue_sets,
                                        ipuz_clue_sets_get_direction (priv->clue_sets, i));
      if (clues->len > 0)
        {
          flags |= IPUZ_PUZZLE_FLAG_HAS_CLUES;
          break;
        }
    }

  if (priv->uses_extensions)
    flags |= IPUZ_PUZZLE_FLAG_USES_EXTENSIONS;

  if (priv->has_solution)
    flags |= IPUZ_PUZZLE_FLAG_HAS_SOLUTION;

  if (priv->has_saved)
    flags |= IPUZ_PUZZLE_FLAG_HAS_SAVED;

  return flags;
}

/* Used for calculate_info */
typedef struct
{
  IPuzPuzzle *puzzle;
  IPuzPuzzleInfo *info;
  IPuzCharsetBuilder *solution_chars_builder;
  IPuzCharsetBuilder *clue_lengths_builder;
} InfoTuple;

static void
calculate_cells_foreach_cb (IPuzCrossword *crossword,
                            IPuzCell      *cell,
                            IPuzCellCoord  coord,
                            gpointer       user_data)
{
  InfoTuple *info_tuple = user_data;

  if (IPUZ_CELL_IS_BLOCK (cell))
    info_tuple->info->cell_stats.block_count ++;
  else if (IPUZ_CELL_IS_NORMAL (cell))
    info_tuple->info->cell_stats.normal_count ++;
  else if (IPUZ_CELL_IS_NULL (cell))
    info_tuple->info->cell_stats.null_count ++;
  else
    g_assert_not_reached ();

  if (IPUZ_CELL_IS_NORMAL (cell))
    {
      const gchar *solution;

      solution = ipuz_cell_get_solution (cell);

      if (solution)
        ipuz_charset_builder_add_text (info_tuple->solution_chars_builder,
                                       solution);
    }
}

static void
calculate_clues_foreach_cb (IPuzClueDirection  direction,
                            IPuzClue          *clue,
                            IPuzClueId         clue_id,
                            gpointer           user_data)
{
  InfoTuple *info_tuple = user_data;
  const GArray *cells;

  /* Only set this flag if there is text for a clue */
  if (ipuz_clue_get_clue_text (clue))
    info_tuple->info->flags |= IPUZ_PUZZLE_FLAG_HAS_CLUES;

  cells = ipuz_clue_get_cells (clue);
  if (cells)
    ipuz_charset_builder_add_character (info_tuple->clue_lengths_builder,
                                        (gunichar) cells->len);
}

static void
calculate_flags (IPuzPuzzle     *puzzle,
                 IPuzPuzzleInfo *info)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle));

  if (priv->uses_extensions)
    info->flags |= IPUZ_PUZZLE_FLAG_USES_EXTENSIONS;

  if (priv->has_solution)
    info->flags |= IPUZ_PUZZLE_FLAG_HAS_SOLUTION;

  if (priv->has_saved)
    info->flags |= IPUZ_PUZZLE_FLAG_HAS_SAVED;
}

static void
calculate_pangram (IPuzPuzzle     *puzzle,
                   IPuzPuzzleInfo *info)
{
  IPuzCharsetIter *iter;
  g_autoptr (IPuzCharset) charset = NULL;

  info->pangram_count = G_MAXUINT;

  charset = ipuz_puzzle_get_charset (puzzle);
  for (iter = ipuz_charset_iter_first (charset);
       iter;
       iter = ipuz_charset_iter_next (iter))
    {
      IPuzCharsetIterValue value;

      value = ipuz_charset_iter_get_value (iter);
      info->pangram_count =
        MIN (info->pangram_count, ipuz_charset_get_char_count (info->solution_chars, value.c));
    }

  if (info->pangram_count == G_MAXUINT)
    info->pangram_count = 0;
}

static void
ipuz_crossword_calculate_info (IPuzPuzzle     *puzzle,
                               IPuzPuzzleInfo *info)
{
  InfoTuple info_tuple;

  g_assert (IPUZ_IS_PUZZLE (puzzle));
  g_assert (IPUZ_IS_PUZZLE_INFO (info));

  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->calculate_info (puzzle, info);

  info_tuple.puzzle = puzzle;
  info_tuple.info = info;
  info_tuple.solution_chars_builder = ipuz_charset_builder_new ();
  info_tuple.clue_lengths_builder = ipuz_charset_builder_new ();

  ipuz_crossword_foreach_cell (IPUZ_CROSSWORD (puzzle),
                               calculate_cells_foreach_cb,
                               &info_tuple);
  info->solution_chars = ipuz_charset_builder_build (info_tuple.solution_chars_builder);

  ipuz_crossword_foreach_clue (IPUZ_CROSSWORD (puzzle),
                               calculate_clues_foreach_cb,
                               &info_tuple);
  info->clue_lengths = ipuz_charset_builder_build (info_tuple.clue_lengths_builder);

  calculate_pangram (puzzle, info);
  calculate_flags (puzzle, info);
}

/*
 * Copying boards is painful. They don't exist on their own but refer
 * to other portions of the crossword. There's not really a good way to
 * just make fresh copies of cells, as they refer to styles and clues
 * within the puzzle. We can copy the board and cells on the surface,
 * then refix them up afterwards. Regardless of which, Boards don't
 * exist outside the context of their Crossword in practice.
 */
static void
ipuz_crossword_deep_clone_board (IPuzCrossword *src,
                                 IPuzCrossword *dest)
{
  IPuzCrosswordPrivate *dest_priv;
  GHashTable *styles = NULL;
  guint row, column;

  dest_priv = ipuz_crossword_get_instance_private (dest);

  ipuz_board_resize (dest_priv->board, dest_priv->width, dest_priv->height);
  g_object_get (G_OBJECT (dest),
                "styles", &styles,
                NULL);

  for (row = 0; row < (guint) dest_priv->height; row++)
    {
      for (column = 0; column < (guint) dest_priv->width; column++)
        {
          IPuzCellCoord coord = { .row = row, .column = column };
          IPuzCell *src_cell, *dest_cell;
          IPuzStyle *style;

          src_cell = ipuz_crossword_get_cell (src, coord);
          dest_cell = ipuz_crossword_get_cell (dest, coord);

          dest_cell->cell_type = src_cell->cell_type;
          dest_cell->number = src_cell->number;
          dest_cell->label = g_strdup (src_cell->label);
          dest_cell->solution = g_strdup (src_cell->solution);
          dest_cell->initial_val = g_strdup (src_cell->initial_val);
          dest_cell->saved_guess = g_strdup (src_cell->saved_guess);
          dest_cell->style_name = g_strdup (src_cell->style_name);

          if (dest_cell->style_name && styles)
            {
              style = g_hash_table_lookup (styles, dest_cell->style_name);
              if (style)
                dest_cell->style = ipuz_style_ref (style);
            }

          /* update the clues. We will have already copied the clues before we attempt to do this */
	  if (dest_cell->clues == NULL)
            dest_cell->clues = g_array_new (FALSE, TRUE, sizeof (IPuzClue *));

	  if (src_cell->clues)
	    {
	      for (guint i = 0; i < src_cell->clues->len; i++)
	        {
	          IPuzClueId clue_id;
	          IPuzClue *src_clue, *dest_clue;

	          src_clue = g_array_index (src_cell->clues, IPuzClue *, i);
		  clue_id.direction = src_clue->direction;
	          clue_id.index = ipuz_crossword_find_clue (src, src_clue);
	          dest_clue = ipuz_crossword_get_clue_by_id (dest, clue_id);
	          g_array_append_val (dest_cell->clues, dest_clue);
	        }
	    }

          /* There's a chance the original crossword has a weird state
           * with a style_name and an independent style. We keep that
           * inconsistent state rather than trying to clean it up. */
          if (dest_cell->style == NULL)
            dest_cell->style = ipuz_style_copy (src_cell->style);
        }
    }
}

static void
ipuz_crossword_clone (IPuzPuzzle *src,
                      IPuzPuzzle *dest)
{
  IPuzCrosswordPrivate *src_priv, *dest_priv;

  if (src == NULL)
    return;

  g_assert (src != NULL);

  src_priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (src));
  dest_priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (dest));

  /* Chain up to start to get the meta information */
  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->clone (src, dest);

  dest_priv->width = src_priv->width;
  dest_priv->height = src_priv->height;
  dest_priv->showenumerations = src_priv->showenumerations;
  dest_priv->clue_placement = src_priv->clue_placement;


  ipuz_clue_sets_clone (src_priv->clue_sets, dest_priv->clue_sets);
  ipuz_crossword_deep_clone_board (IPUZ_CROSSWORD (src),
                                   IPUZ_CROSSWORD (dest));
  dest_priv->guesses = ipuz_guesses_copy (src_priv->guesses);

  dest_priv->uses_extensions = src_priv->uses_extensions;
  dest_priv->has_solution = src_priv->has_solution;
  dest_priv->has_saved = src_priv->has_saved;
}

static const gchar *const *
ipuz_crossword_get_kind_str (IPuzPuzzle *puzzle)
{
  static const char *kind_str[] =
    {
      "http://ipuz.org/crossword#1",
      NULL
    };

  return kind_str;
}


typedef struct
{
  gpointer old_style;
  gpointer new_style;
} StyleSwapTuple;

static void
foreach_fixup_style (IPuzCrossword *crossword,
                     IPuzCell      *cell,
                     IPuzCellCoord  coord,
                     gpointer       user_data)
{
  StyleSwapTuple *tuple;

  tuple = (StyleSwapTuple *) user_data;

  if (cell->style == tuple->old_style)
    ipuz_cell_set_style (cell, tuple->new_style, NULL);
}

static void
ipuz_crossword_set_style (IPuzPuzzle  *puzzle,
                          const gchar *style_name,
                          IPuzStyle   *style)
{
  StyleSwapTuple tuple;

  tuple.old_style = ipuz_puzzle_get_style (puzzle, style_name);
  tuple.new_style = style;

  /* old_style may be unreffed after this call. Do not do anything
   * with it */
  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->set_style (puzzle, style_name, style);

  if (tuple.old_style != NULL)
    ipuz_crossword_foreach_cell (IPUZ_CROSSWORD (puzzle),
                                 foreach_fixup_style, &tuple);
}

static gboolean
ipuz_crossword_equal (IPuzPuzzle *puzzle_a,
                      IPuzPuzzle *puzzle_b)
{
  IPuzCrosswordPrivate *priv_a, *priv_b;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (puzzle_b), FALSE);

  priv_a = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle_a));
  priv_b = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle_b));

  /* clue_sets can never be NULL because it is initialized in
   * ipuz_crossword_init_clues */
  if (! ipuz_clue_sets_equal (priv_a->clue_sets, priv_b->clue_sets))
    return FALSE;

  return (priv_a->width == priv_b->width
          && priv_a->height == priv_b->height
          && priv_a->showenumerations == priv_b->showenumerations
          && ipuz_board_equal (priv_a->board, priv_b->board)
          && ipuz_guesses_equal (priv_a->guesses, priv_b->guesses)
          && priv_a->uses_extensions == priv_b->uses_extensions
          && priv_a->has_solution == priv_b->has_solution
          && priv_a->has_saved == priv_b->has_saved
          && priv_a->clue_placement == priv_b->clue_placement
          && IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->equal (puzzle_a, puzzle_b));
}

static gboolean
ipuz_crossword_real_clue_continues_up (IPuzCrossword *self,
                                       IPuzCellCoord  coord)
{
  if (coord.row > 0)
    {
      IPuzCellCoord neighbor_coord = coord;
      IPuzCell *neighbor_cell;
      neighbor_coord.row --;
      neighbor_cell = ipuz_crossword_get_cell (self, neighbor_coord);
      return IPUZ_CELL_IS_NORMAL (neighbor_cell);
    }

  return FALSE;
}

static gboolean
ipuz_crossword_real_clue_continues_down (IPuzCrossword *self,
                                         IPuzCellCoord  coord)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (self);

  if (coord.row < (guint)priv->height - 1)
    {
      IPuzCellCoord neighbor_coord = coord;
      IPuzCell *neighbor_cell;
      neighbor_coord.row ++;
      neighbor_cell = ipuz_crossword_get_cell (self, neighbor_coord);
      return IPUZ_CELL_IS_NORMAL (neighbor_cell);
    }

  return FALSE;
}

static gboolean
ipuz_crossword_real_clue_continues_left (IPuzCrossword *self,
                                         IPuzCellCoord  coord)
{
  if (coord.column > 0)
    {
      IPuzCellCoord neighbor_coord = coord;
      IPuzCell *neighbor_cell;
      neighbor_coord.column --;
      neighbor_cell = ipuz_crossword_get_cell (self, neighbor_coord);
      return IPUZ_CELL_IS_NORMAL (neighbor_cell);
    }

  return FALSE;
}

static gboolean
ipuz_crossword_real_clue_continues_right (IPuzCrossword *self,
                                          IPuzCellCoord  coord)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (self);

  if (coord.column < (guint)priv->width - 1)
    {
      IPuzCellCoord neighbor_coord = coord;
      IPuzCell *neighbor_cell;
      neighbor_coord.column ++;
      neighbor_cell = ipuz_crossword_get_cell (self, neighbor_coord);
      return IPUZ_CELL_IS_NORMAL (neighbor_cell);
    }

  return FALSE;
}

static void
ipuz_crossword_real_mirror_cell (IPuzCrossword      *self,
                                 IPuzCellCoord       src_coord,
                                 IPuzCellCoord       dest_coord,
                                 IPuzSymmetry        symmetry,
                                 IPuzSymmetryOffset  symmetry_offset)
{
  IPuzCell *src, *dest;

  src = ipuz_crossword_get_cell (self, src_coord);
  dest = ipuz_crossword_get_cell (self, dest_coord);

  ipuz_cell_set_cell_type (dest, ipuz_cell_get_cell_type (src));
}

static gboolean
ipuz_crossword_real_check_mirror (IPuzCrossword      *self,
                                  IPuzCellCoord       src_coord,
                                  IPuzCellCoord       target_coord,
                                  IPuzSymmetry        symmetry,
                                  IPuzSymmetryOffset  symmetry_offset)
{
  IPuzCell *src, *target;

  src = ipuz_crossword_get_cell (self, src_coord);
  target = ipuz_crossword_get_cell (self, target_coord);

  return (ipuz_cell_get_cell_type (src) == ipuz_cell_get_cell_type (target));
}

static void
ipuz_crossword_real_fix_symmetry  (IPuzCrossword *self,
                                   IPuzSymmetry   symmetry,
                                   GArray        *coords)
{
  IPuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (self);

  /* Nothing to do */
  if (symmetry == IPUZ_SYMMETRY_NONE)
    return;

  /* Quarter-symmetric only makes sense if we're square */
  if (priv->width != priv->height)
    g_return_if_fail (symmetry != IPUZ_SYMMETRY_ROTATIONAL_QUARTER);

  for (guint i = 0; i < coords->len; i++)
    {
      IPuzCellCoord coord;
      IPuzCellCoord mirror_coord;

      coord = g_array_index (coords, IPuzCellCoord, i);

      mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height,
                                              symmetry, IPUZ_SYMMETRY_OFFSET_OPPOSITE);
      ipuz_crossword_mirror_cell (self, coord, mirror_coord, symmetry, IPUZ_SYMMETRY_OFFSET_OPPOSITE);

      /* Add the other two points as well */
      if (symmetry == IPUZ_SYMMETRY_ROTATIONAL_QUARTER ||
          symmetry == IPUZ_SYMMETRY_MIRRORED)
        {
          mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height,
                                                  symmetry, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT);
          ipuz_crossword_mirror_cell (self, coord, mirror_coord, symmetry, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT);

          mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height,
                                                  symmetry, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT);
          ipuz_crossword_mirror_cell (self, coord, mirror_coord, symmetry, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT);
        }
    }
}

static void
ipuz_crossword_real_fix_numbering (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;
  guint row, col;
  gint number = 1;

  priv = ipuz_crossword_get_instance_private (self);

  for (row = 0; row < (guint) priv->height; row++)
    {
      for (col = 0; col < (guint) priv->width; col++)
        {
          IPuzCellCoord coord = { .row = row, .column = col };
          IPuzCell *cell;

          cell = ipuz_crossword_get_cell (self, coord);

          if (! IPUZ_CELL_IS_NORMAL (cell))
            continue;

          /* Clear the label */
          ipuz_cell_set_label (cell, NULL);

          if ((!ipuz_crossword_clue_continues_up (self, coord) && ipuz_crossword_clue_continues_down (self, coord)) ||
              (!ipuz_crossword_clue_continues_left (self, coord) && ipuz_crossword_clue_continues_right (self, coord)))
            {
              ipuz_cell_set_number (cell, number);
              number++;
            }
          else
            {
              ipuz_cell_set_number (cell, 0);
            }
        }
    }
}

static void
match_clue_sets (IPuzClueSets *src_clue_sets,
                 IPuzClueSets *dest_clue_sets)
{
  g_assert (src_clue_sets);
  g_assert (dest_clue_sets);

  for (guint n = 0; n < ipuz_clue_sets_get_n_clue_sets (src_clue_sets); n++)
    {
      IPuzClueDirection direction;
      GArray *src_clues;
      GArray *dest_clues;

      direction = ipuz_clue_sets_get_direction (src_clue_sets, n);
      src_clues = ipuz_clue_sets_get_clues (src_clue_sets, direction);
      dest_clues = ipuz_clue_sets_get_clues (dest_clue_sets, direction);

      g_assert (src_clues != NULL);
      if (dest_clues == NULL)
        continue;

      for (guint i = 0; i < src_clues->len; i++)
        {
          IPuzClue *src_clue;
          const GArray *src_cells;

          src_clue = g_array_index (src_clues, IPuzClue *, i);
          src_cells = ipuz_clue_get_cells (src_clue);

          for (guint j = 0; j < dest_clues->len; j++)
            {
              IPuzClue *dest_clue;
              const GArray *dest_cells;

              dest_clue = g_array_index (dest_clues, IPuzClue *, j);
              dest_cells = ipuz_clue_get_cells (dest_clue);

              if ((src_cells->len == dest_cells->len) &&
                  (memcmp (src_cells->data, dest_cells->data, src_cells->len * sizeof (IPuzCellCoord)) == 0))
                {
                  IPuzEnumeration *enumeration = ipuz_clue_get_enumeration (src_clue);
                  /* We don't need to set the label, number, direction, or
                   * location as we will have built that already in
                   * fix_clues () */
                  ipuz_clue_set_clue_text (dest_clue, ipuz_clue_get_clue_text (src_clue));
                  ipuz_clue_set_enumeration (dest_clue, enumeration);
                  ipuz_enumeration_unref (enumeration);
                }
            }
        }
    }
}

static void
ipuz_crossword_real_fix_clues (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;
  g_autoptr (IPuzClueSets) old_clue_sets = NULL;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  priv = ipuz_crossword_get_instance_private (self);

  /* Keep the old clue_set around to copy over clue labels and
   * enumerations. This comparison is unfortunately n^2, but I don't
   * think there's any way around that. */
  old_clue_sets = priv->clue_sets;
  priv->clue_sets = NULL;
  ipuz_crossword_init_clues (self);

  for (guint row = 0; row < (guint) priv->height; row++)
    {
      for (guint col = 0; col < (guint) priv->width; col++)
        {
          IPuzCell *cell;
          IPuzCellCoord coord = { .row = row, .column = col };
          gint number;

          cell = ipuz_crossword_get_cell (self, coord);
	  /* Clear out the old clues */
	  ipuz_cell_clear_clues (cell);
          number = ipuz_cell_get_number (cell);

          if (number > 0)
            {
              IPuzClue *across_clue;
              IPuzClue *down_clue;

              across_clue = calculate_clue (self, IPUZ_CLUE_DIRECTION_ACROSS, coord, number);
              down_clue = calculate_clue (self, IPUZ_CLUE_DIRECTION_DOWN, coord, number);

              if (across_clue)
                ipuz_clue_sets_append_clue (priv->clue_sets, IPUZ_CLUE_DIRECTION_ACROSS, across_clue);
              if (down_clue)
                ipuz_clue_sets_append_clue (priv->clue_sets, IPUZ_CLUE_DIRECTION_DOWN, down_clue);
            }
        }
    }

  /* Set up all the cells for the clues we just created */
  ipuz_crossword_fixup_clues (self);
  match_clue_sets (old_clue_sets, priv->clue_sets);
}


static void
ensure_enumeration (IPuzClueDirection  direction,
                    IPuzClue          *clue,
                    IPuzClueId         clue_id,
                    gpointer           user_data)
{
  IPuzEnumeration *enumeration;
  const GArray *cells;
  g_autofree gchar *str = NULL;

  enumeration = ipuz_clue_get_enumeration (clue);
  if (enumeration != NULL)
    {
      ipuz_enumeration_unref (enumeration);
      return;
    }

  /* create an enumeration for clue as if it had no deliminators */
  cells = ipuz_clue_get_cells (clue);
  str = g_strdup_printf ("%u", cells?cells->len:0);
  enumeration = ipuz_enumeration_new (str, IPUZ_VERBOSITY_STANDARD);

  ipuz_clue_set_enumeration (clue, enumeration);
  ipuz_enumeration_unref (enumeration);
}

static void
ipuz_crossword_real_fix_enumerations (IPuzCrossword *self)
{
  gboolean showenumerations = FALSE;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  g_object_get (self,
                "showenumerations", &showenumerations,
                NULL);

  if (!showenumerations)
    return;

  ipuz_crossword_foreach_clue (self, ensure_enumeration, NULL);
}

static void
foreach_fix_styles (IPuzCrossword *crossword,
                    IPuzCell      *cell,
                    IPuzCellCoord  coord,
                    gpointer       user_data)
{
  IPuzStyle *style;

  style = ipuz_cell_get_style (cell);
  if (style && ipuz_style_is_empty (style))
    ipuz_cell_set_style (cell, NULL, NULL);
}

static void
ipuz_crossword_real_fix_styles (IPuzCrossword *self)
{
  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  ipuz_crossword_foreach_cell (self, foreach_fix_styles, NULL);
}

static void
ipuz_crossword_real_fix_all (IPuzCrossword *self,
                             const gchar   *first_attribute_name,
                             va_list        var_args)
{
  const gchar *attribute_name;
  IPuzSymmetry symmetry = IPUZ_SYMMETRY_NONE;
  GArray *symmetry_coords = NULL; /* We don't own this */
  va_list var_args_copy;

  va_copy (var_args_copy, var_args);
  attribute_name = first_attribute_name;

  while (attribute_name)
    {
      if (! g_strcmp0 (attribute_name, "symmetry-coords"))
        {
          if (symmetry_coords != NULL)
            {
              g_warning ("symmetry-coords set multiple times");
              goto out;
            }
          symmetry_coords = va_arg (var_args_copy, GArray *);
        }
      else if (! g_strcmp0 (attribute_name, "symmetry"))
        {
          symmetry = va_arg (var_args_copy, IPuzSymmetry);
        }

      attribute_name = va_arg (var_args_copy, const gchar *);
    }

  /* Apply fixes to the puzzle */
  if (symmetry_coords != NULL)
    ipuz_crossword_fix_symmetry (self, symmetry, symmetry_coords);
  ipuz_crossword_fix_numbering (self);
  ipuz_crossword_fix_clues (self);
  ipuz_crossword_fix_enumerations (self);
  ipuz_crossword_fix_styles (self);

 out:
  va_end (var_args_copy);
}

/**
 * Public Methods
 */

IPuzCrossword *
ipuz_crossword_new (void)
{
  IPuzCrossword *xword;

  xword = g_object_new (IPUZ_TYPE_CROSSWORD, NULL);

  return xword;
}

guint
ipuz_crossword_get_width (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), 0);

  priv = ipuz_crossword_get_instance_private (self);

  return priv->width;
}


guint
ipuz_crossword_get_height (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), 0);

  priv = ipuz_crossword_get_instance_private (self);

  return priv->height;
}


static gboolean
ipuz_crossword_real_set_size (IPuzCrossword *self,
                              gint           width,
                              gint           height)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  priv = ipuz_crossword_get_instance_private (self);

  if (priv->width == width && priv->height == height)
    return FALSE;

  priv->width = width;
  priv->height = height;

  if (priv->width > 0 && priv->height > 0)
    ipuz_board_resize (priv->board, width, height);

  return TRUE;
}

void
ipuz_crossword_set_size   (IPuzCrossword *self,
                           gint           width,
                           gint           height)
{
  g_return_if_fail (IPUZ_IS_CROSSWORD (self));
  g_return_if_fail (width > 0 && height > 0);

  g_object_freeze_notify (G_OBJECT (self));
  if (ipuz_crossword_real_set_size (self, width, height))
    {
      g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_WIDTH]);
      g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_HEIGHT]);
    }
  g_object_thaw_notify (G_OBJECT (self));
}

IPuzBoard *
ipuz_crossword_get_board (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  priv = ipuz_crossword_get_instance_private (self);

  return priv->board;
}

static gboolean
ipuz_crossword_validate_guesses (IPuzCrossword *self,
                                 IPuzGuesses   *guesses)
{
  IPuzCrosswordPrivate *priv;
  guint row, col;

  priv = ipuz_crossword_get_instance_private (self);

  if ((guint) priv->width != ipuz_guesses_get_width (guesses) ||
      (guint) priv->height != ipuz_guesses_get_height (guesses))
    return FALSE;

  for (row = 0; row < (guint) priv->height; row++)
    {
      for (col = 0; col < (guint) priv->width; col++)
        {
          IPuzCell *cell;
          IPuzCellCellType guess_type;
          IPuzCellCoord coord = { .row = row, .column = col };

          cell = ipuz_crossword_get_cell (self, coord);
          guess_type = ipuz_guesses_get_cell_type (guesses, coord);

          /* We check to make sure that if the cell is guessable, we
           * can write to it. We ignore the remainder of the cells. */
          if (IPUZ_CELL_IS_GUESSABLE (cell) &&
              guess_type != IPUZ_CELL_NORMAL)
            return FALSE;
        }
    }
  return TRUE;
}

gboolean
ipuz_crossword_set_guesses (IPuzCrossword *self,
                            IPuzGuesses   *guesses)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  priv = ipuz_crossword_get_instance_private (self);

  if (guesses)
    {
      if (! ipuz_crossword_validate_guesses (self, guesses))
        return FALSE;
      ipuz_guesses_ref (guesses);
    }

  g_clear_pointer (&priv->guesses, ipuz_guesses_unref);
  priv->guesses = guesses;
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_GUESSES]);

  return TRUE;
}

IPuzGuesses *
ipuz_crossword_get_guesses (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  priv = ipuz_crossword_get_instance_private (self);

  return priv->guesses;
}

/* Convenience function. */
IPuzCell *
ipuz_crossword_get_cell (IPuzCrossword *self,
                         IPuzCellCoord  coord)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  priv = ipuz_crossword_get_instance_private (self);
  g_return_val_if_fail (priv->board != NULL, NULL);

  return ipuz_board_get_cell (priv->board, coord);
}


/* Foreach functions */
void
ipuz_crossword_foreach_clue (IPuzCrossword       *self,
                             IPuzClueForeachFunc  func,
                             gpointer             data)
{
  IPuzCrosswordPrivate *priv;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  for (guint i = 0; i < ipuz_clue_sets_get_n_clue_sets (priv->clue_sets); i++)
    {
      IPuzClueDirection direction;
      GArray *clues;

      direction = ipuz_clue_sets_get_direction (priv->clue_sets, i);
      clues = ipuz_clue_sets_get_clues (priv->clue_sets, direction);

      for (guint j = 0; j < clues->len; j++)
        {
          IPuzClue *clue;
          IPuzClueId clue_id = {
            .direction = direction,
            .index = j,
          };

          clue = g_array_index (clues, IPuzClue *, j);
          (func) (direction, clue, clue_id, data);
        }
    }
}

void
ipuz_crossword_foreach_cell (IPuzCrossword                *self,
                             IPuzCrosswordForeachCellFunc  func,
                             gpointer                      user_data)
{
  IPuzCrosswordPrivate *priv;
  guint row, col;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  priv = ipuz_crossword_get_instance_private (self);

  for (row = 0; row < (guint) priv->height; row++)
    {
      for (col = 0; col < (guint) priv->width; col++)
        {
          IPuzCell *cell;
          IPuzCellCoord coord = { .row = row, .column = col };

          cell = ipuz_crossword_get_cell (self, coord);
          (func) (self, cell, coord, user_data);
        }
    }
}

guint
ipuz_crossword_get_n_clue_sets (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), 0);

  priv = ipuz_crossword_get_instance_private (self);

  return ipuz_clue_sets_get_n_clue_sets (priv->clue_sets);
}

IPuzClueDirection
ipuz_crossword_clue_set_get_dir (IPuzCrossword *self,
                                 guint          index)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), IPUZ_CLUE_DIRECTION_NONE);

  priv = ipuz_crossword_get_instance_private (self);

  return ipuz_clue_sets_get_direction (priv->clue_sets, index);
}

const gchar *
ipuz_crossword_clue_set_get_label (IPuzCrossword     *self,
                                   IPuzClueDirection  direction)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  priv = ipuz_crossword_get_instance_private (self);

  return ipuz_clue_sets_get_label (priv->clue_sets, direction);
}

GArray *
ipuz_crossword_get_clues (IPuzCrossword     *self,
                          IPuzClueDirection  direction)
{
  IPuzCrosswordPrivate *priv;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  priv = ipuz_crossword_get_instance_private (self);

  return ipuz_clue_sets_get_clues (priv->clue_sets, direction);
}

IPuzClueId
ipuz_crossword_get_clue_id (IPuzCrossword  *self,
                            const IPuzClue *clue)
{
  IPuzClueId id = {
    .direction = IPUZ_CLUE_DIRECTION_NONE,
    .index = 0,
  };

  if (clue)
    {
      gboolean found = FALSE;
      GArray *clues;
      guint i;

      clues = ipuz_crossword_get_clues (self, clue->direction);
      if (clues == NULL)
        return id;

      for (i = 0; i < clues->len; i++)
        {
          IPuzClue *tmp_clue = g_array_index (clues, IPuzClue *, i);
          if (ipuz_clue_equal (clue, tmp_clue))
            {
              found = TRUE;
              break;
            }
        }

      if (found)
        {
          id.direction = clue->direction;
          id.index = i;
        }
    }

  return id;
}

IPuzClue *
ipuz_crossword_get_clue_by_id (IPuzCrossword *self,
                               IPuzClueId     clue_id)
{
  GArray *clues;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);
  g_return_val_if_fail (! IPUZ_CLUE_ID_IS_UNSET (clue_id), NULL);

  clues = ipuz_crossword_get_clues (self, clue_id.direction);

  if (clues && clue_id.index < clues->len)
    return g_array_index (clues, IPuzClue *, clue_id.index);

  return NULL;
}


static void
unlink_clue_from_cells (IPuzCrossword *self,
                        IPuzClue      *clue)
{
  if (clue->cells == NULL)
    return;

  for (guint i = 0; i < clue->cells->len; i++)
    {
      IPuzCell *cell;
      IPuzCellCoord coord;

      coord = g_array_index (clue->cells, IPuzCellCoord, i);
      cell = ipuz_crossword_get_cell (self, coord);
      if (cell)
        ipuz_cell_clear_clue_direction (cell, clue->direction);
    }
}

void
ipuz_crossword_unlink_clue (IPuzCrossword *self,
                            IPuzClue      *clue)
{
  IPuzCrosswordPrivate *priv;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));
  g_return_if_fail (clue != NULL);

  priv = ipuz_crossword_get_instance_private (self);

  //  g_warning ("this function won't work correctly until we add refcounting to clues\n");
  unlink_clue_from_cells (self, clue);
  ipuz_clue_sets_remove_clue (priv->clue_sets, clue->direction, clue, TRUE);
}

guint
ipuz_crossword_get_n_clues (IPuzCrossword     *self,
                            IPuzClueDirection  direction)
{
  GArray *clues;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), 0);

  clues = ipuz_crossword_get_clues (self, direction);
  if (clues)
    return clues->len;

  return 0;
}

guint
ipuz_crossword_find_clue (IPuzCrossword *self,
                          IPuzClue      *clue)
{
  GArray *clues = NULL;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), 0);
  g_return_val_if_fail (clue != NULL, 0);

  clues = ipuz_crossword_get_clues (self, clue->direction);

  for (guint i = 0; i < clues->len; i ++)
    {
      IPuzClue *tmp_clue = g_array_index (clues, IPuzClue *, i);
      if (ipuz_clue_equal (clue, tmp_clue))
        return i;
    }

  return 0;
}

static gchar *
ipuz_crossword_get_string (IPuzCrossword *self,
                           IPuzClueId     clue_id,
                           gboolean       guesses)
{
  IPuzCrosswordPrivate *priv;
  IPuzClue *clue;
  const GArray *cells;
  GString *string;
  guint i;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  priv = ipuz_crossword_get_instance_private (self);
  clue = ipuz_crossword_get_clue_by_id (self, clue_id);

  if (clue == NULL)
    return NULL;

  string = g_string_new (NULL);
  cells = ipuz_clue_get_cells (clue);
  for (i = 0; i < cells->len; i++)
    {
      IPuzCellCoord coord;
      const char *solution = NULL;

      coord = g_array_index (cells, IPuzCellCoord, i);
      if (guesses)
        {
          solution = ipuz_guesses_get_guess (priv->guesses, coord);
        }
      else
        {
          IPuzCell *cell;

          cell = ipuz_crossword_get_cell (self, coord);
          solution = ipuz_cell_get_solution (cell);
        }

      if (solution && solution[0] !='\0')
        g_string_append (string, solution);
      else
        g_string_append (string, "?");
    }

  return g_string_free (string, FALSE);
}


/**
 * ipuz_crossword_get_clue_string_by_id:
 * @self: An 'IPuzCrossword'
 * @direction: Direction of the clue
 * @number: index of the clue
 *
 * Returns a string containing the solution of the puzzle for a given
 * clue. This string will have '?' characterd embedded within it if
 * there are cells without solutions set yet.
 *
 * Returns: The solution for the puzzle for Clue, or %NULL
 **/
gchar *
ipuz_crossword_get_clue_string_by_id (IPuzCrossword *self,
                                      IPuzClueId     clue_id)
{
  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  return ipuz_crossword_get_string (self, clue_id, FALSE);
}

/**
 * ipuz_crossword_get_guess_string_by_id:
 * @self: An 'IPuzCrossword'
 * @direction: Direction of the clue
 * @number: index of the clue
 *
 * Returns a string containing the guess in the puzzle for a given
 * clue. This string will have '?' characterd embedded within it if
 * there are cells not completely filled out.
 *
 * Returns: The solution for the puzzle for Clue, or %NULL
 **/
gchar *
ipuz_crossword_get_guess_string_by_id (IPuzCrossword *self,
                                       IPuzClueId     clue_id)
{
  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  return ipuz_crossword_get_string (self, clue_id, TRUE);
}

/**
 * ipuz_crossword_clue_guessed:
 * @self:  An 'IPuzCrossword'
 * @clue: The clue to check
 *
 * Check to see if every field in the clue is guessed. This doesn't
 * account for the correctness of the clue
 *
 * Returns: %TRUE, if every field in the clue is guessed.
 **/
gboolean
ipuz_crossword_clue_guessed (IPuzCrossword *self,
                             IPuzClue      *clue,
                             gboolean      *correct)
{
  IPuzCrosswordPrivate *priv;
  const GArray *cells;
  guint i;
  gboolean guessed = TRUE;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);
  g_return_val_if_fail (clue != NULL, FALSE);

  priv = ipuz_crossword_get_instance_private (self);
  cells = ipuz_clue_get_cells (clue);

  if (cells == NULL || cells->len == 0)
    return FALSE;
  if (priv->guesses == NULL)
    return FALSE;

  if (correct)
    *correct = TRUE;

  for (i = 0; i < cells->len; i++)
    {
      IPuzCellCoord coord;
      IPuzCell *cell;
      const char *guess = NULL;
      const char *solution = NULL;

      coord = g_array_index (cells, IPuzCellCoord, i);
      cell = ipuz_crossword_get_cell (self, coord);

      if (ipuz_cell_get_initial_val (cell))
        continue;
      guess = ipuz_guesses_get_guess (priv->guesses, coord);
      solution = ipuz_cell_get_solution (cell);

      if (guess == NULL || guess[0] == '\0')
        guessed = FALSE;
      if (correct)
        *correct = !g_strcmp0 (solution, guess) && *correct;
    }

  return guessed;
}

IPuzClue *
ipuz_crossword_find_clue_by_number (IPuzCrossword     *self,
                                    IPuzClueDirection  direction,
                                    gint               number)
{
  GArray *clues = NULL;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  clues = ipuz_crossword_get_clues (self, direction);

  for (guint i = 0; i < clues->len; i ++)
    {
      IPuzClue *clue;
      clue = g_array_index (clues, IPuzClue *, i);
      if (clue->number == number)
        return clue;
    }
  return NULL;
}

IPuzClue *
ipuz_crossword_find_clue_by_label (IPuzCrossword     *self,
                                   IPuzClueDirection  direction,
                                   const gchar       *label)
{
  GArray *clues = NULL;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);
  g_return_val_if_fail (label != NULL, NULL);

  clues = ipuz_crossword_get_clues (self, direction);

  for (guint i = 0; i < clues->len; i ++)
    {
      IPuzClue *clue;
      clue = g_array_index (clues, IPuzClue *, i);
      if (g_strcmp0 (label, clue->label) == 0)
        return clue;
    }
  return NULL;
}

IPuzClue *
ipuz_crossword_find_clue_by_coord (IPuzCrossword     *self,
                                   IPuzClueDirection  direction,
                                   IPuzCellCoord      coord)
{
 GArray *clues = NULL;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  clues = ipuz_crossword_get_clues (self, direction);
  if (clues == NULL)
    return NULL;

  for (guint i = 0; i < clues->len; i ++)
    {
      IPuzClue *clue;
      clue = g_array_index (clues, IPuzClue *, i);
      if (ipuz_clue_contains_cell (clue, coord))
        return clue;
    }
  return NULL;
}

static gboolean
ipuz_crossword_game_won_solution (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;
  guint row, col;

  priv = ipuz_crossword_get_instance_private (self);

  for (row = 0; row < (guint) priv->height; row++)
    {
      for (col = 0; col < (guint) priv->width; col++)
        {
          IPuzCell *cell;
          IPuzCellCoord coord = { .row = row, .column = col };

          cell = ipuz_crossword_get_cell (self, coord);
          if (IPUZ_CELL_IS_GUESSABLE (cell))
            {
              const char *guess = ipuz_guesses_get_guess (priv->guesses, coord);
              /* FIXME(charset): There are puzzles where the solution charset
               * doesn't match the solution and we have a bunch of
               * blank solution values. We want to catch that case.
               */
              /* FIXME(charset): should we normalize guess and solution first?*/
              if (guess == NULL)
                return FALSE;
              if (g_strcmp0 (guess, cell->solution))
                return FALSE;
            }
        }
    }

  return TRUE;
}

gboolean
ipuz_crossword_game_won (IPuzCrossword *self)
{
  guint flags;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  /* You can't win without playing! */
  if (ipuz_crossword_get_guesses (self) == NULL)
    return FALSE;

  flags = ipuz_puzzle_get_flags (IPUZ_PUZZLE (self));

  if (flags & IPUZ_PUZZLE_FLAG_HAS_SOLUTION)
    return ipuz_crossword_game_won_solution (self);

  if (flags & IPUZ_PUZZLE_FLAG_HAS_CHECKSUM)
    {
      /* FIXME(checksum): need to support this! */
    }
  return FALSE;
}


static void
solution_chars_foreach_cb (IPuzCrossword *crossword,
                           IPuzCell      *cell,
                           IPuzCellCoord  coord,
                           gpointer       user_data)
{
  IPuzCharsetBuilder *builder = user_data;

  ipuz_charset_builder_add_text (builder, ipuz_cell_get_solution (cell));
}

IPuzCharset *
ipuz_crossword_get_solution_chars (IPuzCrossword *self)
{
  IPuzCharsetBuilder *builder;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  builder = ipuz_charset_builder_new ();
  ipuz_crossword_foreach_cell (self, solution_chars_foreach_cb, builder);

  return ipuz_charset_builder_build (builder);
}

static IPuzClue *
calculate_clue (IPuzCrossword     *self,
                IPuzClueDirection  direction,
                IPuzCellCoord      coord,
                gint               number)
{
  IPuzClue *clue;
  IPuzCell *cell;
  gboolean valid = FALSE;
  guint len = 0;

  /* If we're already in a clue, we don't want to create another one */
  if (ipuz_crossword_find_clue_by_coord (self, direction, coord) != NULL)
    return NULL;

  /* create a perspective new clue, and see if it's valid. We do this
   * by seeing if we can walk in direction and append the cell to the
   * clue. If our len is > 1, then it means that it's a valid
   * clue. Also, we do a dance between NORMAL cells and GESSABLE cells
   * because we can have a row of all guessable cells that shouldn't
   * be calculated. See the first puzzle as an example of this. */
  clue = ipuz_clue_new ();
  ipuz_clue_set_direction (clue, direction);
  ipuz_clue_set_number (clue, number);

  cell = ipuz_crossword_get_cell (self, coord);
  while (IPUZ_CELL_IS_NORMAL (cell))
    {
      ipuz_clue_append_cell (clue, coord);
      len ++;
      if (IPUZ_CELL_IS_GUESSABLE (cell))
        valid = TRUE;

      if (direction == IPUZ_CLUE_DIRECTION_ACROSS &&
          !ipuz_crossword_clue_continues_right (self, coord))
        break;

      if (direction == IPUZ_CLUE_DIRECTION_DOWN &&
          !ipuz_crossword_clue_continues_down (self, coord))
        break;

      if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
        coord.column ++;
      else
        coord.row ++;
      cell = ipuz_crossword_get_cell (self, coord);
    }

  if (valid && len > 1)
    return clue;

  ipuz_clue_free (clue);
  return NULL;
}

/**
 * ipuz_crossword_get_symmetry:
 * @self: A `IPuzCrossword`
 *
 * Calculates the symmetry of @self. Note, there can be multiple valid
 * calculations for a board. For example, we can't say anything at all
 * about the symmetry for a blank, square board. This function returns
 * the first one that matches.
 *
 * Returns: The apparent symmetry of the puzzle.
 **/
IPuzSymmetry
ipuz_crossword_get_symmetry (IPuzCrossword *self)
{
  IPuzCrosswordPrivate *priv;
  guint row, col;
  /* We assume these are true until proved otherwise */
  gboolean is_half_rotational = TRUE;
  gboolean is_quarter_rotational = TRUE;
  gboolean is_horizontal = TRUE;
  gboolean is_vertical = TRUE;
  gboolean is_mirrored = TRUE;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), IPUZ_SYMMETRY_NONE);

  priv = ipuz_crossword_get_instance_private (self);

  /* Quarter-symmetric only makes sense if we're square */
  if (priv->width != priv->height)
    is_quarter_rotational = FALSE;

  for (row = 0; row < (guint) priv->height; row++)
    {
      for (col = 0; col < (guint) priv->width; col++)
        {
          IPuzCellCoord coord = { .row = row, .column = col };
          IPuzCellCoord mirror_coord;

          if (is_half_rotational)
            {
              mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height, IPUZ_SYMMETRY_ROTATIONAL_HALF, IPUZ_SYMMETRY_OFFSET_OPPOSITE);
              if (! ipuz_crossword_check_mirror (self, coord, mirror_coord, IPUZ_SYMMETRY_ROTATIONAL_HALF, IPUZ_SYMMETRY_OFFSET_OPPOSITE))
                {
                  is_half_rotational = FALSE;
                  is_quarter_rotational = FALSE;
                }
            }

          if (is_quarter_rotational)
            {
              mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height, IPUZ_SYMMETRY_ROTATIONAL_QUARTER, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT);
              if (! ipuz_crossword_check_mirror (self, coord, mirror_coord, IPUZ_SYMMETRY_ROTATIONAL_QUARTER, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT))
                is_quarter_rotational = FALSE;

              mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height, IPUZ_SYMMETRY_ROTATIONAL_QUARTER, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT);
              if (! ipuz_crossword_check_mirror (self, coord, mirror_coord, IPUZ_SYMMETRY_ROTATIONAL_QUARTER, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT))
                is_quarter_rotational = FALSE;
            }

          if (is_horizontal)
            {
              mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height, IPUZ_SYMMETRY_HORIZONTAL, IPUZ_SYMMETRY_OFFSET_OPPOSITE);
              if (! ipuz_crossword_check_mirror (self, coord, mirror_coord, IPUZ_SYMMETRY_HORIZONTAL, IPUZ_SYMMETRY_OFFSET_OPPOSITE))
                {
                  is_horizontal = FALSE;
                  is_mirrored = FALSE;
                }
            }

          if (is_vertical)
            {
              mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height, IPUZ_SYMMETRY_VERTICAL, IPUZ_SYMMETRY_OFFSET_OPPOSITE);
              if (! ipuz_crossword_check_mirror (self, coord, mirror_coord, IPUZ_SYMMETRY_VERTICAL, IPUZ_SYMMETRY_OFFSET_OPPOSITE))
                {
                  is_vertical = FALSE;
                  is_mirrored = FALSE;
                }
            }

          if (is_mirrored)
            {
              mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height, IPUZ_SYMMETRY_MIRRORED, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT);
              if (! ipuz_crossword_check_mirror (self, coord, mirror_coord, IPUZ_SYMMETRY_MIRRORED, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT))
                is_mirrored = FALSE;

              mirror_coord = ipuz_symmetry_calculate (coord, priv->width, priv->height, IPUZ_SYMMETRY_MIRRORED, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT);
              if (! ipuz_crossword_check_mirror (self, coord, mirror_coord, IPUZ_SYMMETRY_MIRRORED, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT))
                is_mirrored = FALSE;
            }

          /* We've eliminated everything */
          if (! is_half_rotational &&
              ! is_quarter_rotational &&
              ! is_horizontal &&
              ! is_vertical &&
              ! is_mirrored)
            {
              return IPUZ_SYMMETRY_NONE;
            }
        }
    }

  if (is_quarter_rotational)
    return IPUZ_SYMMETRY_ROTATIONAL_QUARTER;
  else if (is_half_rotational)
    return IPUZ_SYMMETRY_ROTATIONAL_HALF;
  else if (is_mirrored)
    return IPUZ_SYMMETRY_MIRRORED;
  else if (is_horizontal)
    return IPUZ_SYMMETRY_HORIZONTAL;
  else if (is_vertical)
    return IPUZ_SYMMETRY_VERTICAL;
  else
    g_assert_not_reached ();
}

static void
print_clues (GArray   *clues,
             gboolean  showenumerations)
{
  guint i;

  for (i = 0; i < clues->len; i++)
    {
      IPuzClue *clue = g_array_index (clues, IPuzClue *, i);
      g_autoptr (IPuzEnumeration) enumeration = NULL;

      enumeration = ipuz_clue_get_enumeration (clue);
      g_print ("\t");
      if (ipuz_clue_get_number (clue) > 0)
        g_print ("%d. ", ipuz_clue_get_number (clue));
      else if (ipuz_clue_get_label (clue))
        g_print ("%s. ", ipuz_clue_get_label (clue));
      if (ipuz_clue_get_clue_text (clue))
        g_print ("%s ", ipuz_clue_get_clue_text (clue));
      if (showenumerations && enumeration)
        g_print ("(%s)", ipuz_enumeration_get_display (enumeration));
      g_print ("\n");

      if (clue->cells->len > 0)
        {
          g_print ("\tcells: ");
          for (guint j = 0; j < clue->cells->len; j++)
            {
              IPuzCellCoord *coord = & (g_array_index (clue->cells, IPuzCellCoord, j));
              g_print ("[%u, %u] ", coord->row, coord->column);
            }
          g_print ("\n");
        }
    }
}

/* Correction API */

void
ipuz_crossword_fix_symmetry (IPuzCrossword *self,
                             IPuzSymmetry   symmetry,
                             GArray        *coords)
{
  IPuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));
  g_return_if_fail (coords != NULL);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_symmetry (self, symmetry, coords);
}

void
ipuz_crossword_fix_numbering (IPuzCrossword *self)
{
  IPuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_numbering (self);
}

void
ipuz_crossword_fix_clues (IPuzCrossword *self)
{
  IPuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_clues (self);
}

void
ipuz_crossword_fix_enumerations (IPuzCrossword *self)
{
  IPuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_enumerations (self);
}

void
ipuz_crossword_fix_styles (IPuzCrossword *self)
{
  IPuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_styles (self);
}

void
ipuz_crossword_fix_all (IPuzCrossword *self,
                        const char    *first_attribute_name,
                        ...)
{
  IPuzCrosswordClass *klass;
  va_list var_args;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  va_start (var_args, first_attribute_name);
  klass->fix_all (self, first_attribute_name, var_args);
  va_end (var_args);
}

gboolean
ipuz_crossword_clue_continues_up (IPuzCrossword *self,
                                  IPuzCellCoord  coord)
{
  IPuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->clue_continues_up (self, coord);
}

gboolean
ipuz_crossword_clue_continues_down (IPuzCrossword *self,
                                    IPuzCellCoord  coord)
{
  IPuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->clue_continues_down (self, coord);
}

gboolean
ipuz_crossword_clue_continues_left (IPuzCrossword *self,
                                    IPuzCellCoord  coord)
{
  IPuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->clue_continues_left (self, coord);
}

gboolean
ipuz_crossword_clue_continues_right (IPuzCrossword *self,
                                     IPuzCellCoord  coord)
{
  IPuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->clue_continues_right (self, coord);
}

void
ipuz_crossword_mirror_cell (IPuzCrossword      *self,
                            IPuzCellCoord       src_coord,
                            IPuzCellCoord       dest_coord,
                            IPuzSymmetry        symmetry,
                            IPuzSymmetryOffset  symmetry_offset)
{
  IPuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  klass->mirror_cell (self, src_coord, dest_coord, symmetry, symmetry_offset);
}

gboolean
ipuz_crossword_check_mirror (IPuzCrossword      *self,
                             IPuzCellCoord       src_coord,
                             IPuzCellCoord       target_coord,
                             IPuzSymmetry        symmetry,
                             IPuzSymmetryOffset  symmetry_offset)
{
  IPuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->check_mirror (self, src_coord, target_coord, symmetry, symmetry_offset);
}

/* Public Functions */

void
ipuz_crossword_print (IPuzCrossword *self)
{
  g_return_if_fail (IPUZ_CROSSWORD (self));

  /* Puzzle */
  g_autofree gchar *copyright = NULL;
  g_autofree gchar *publisher = NULL;
  g_autofree gchar *publication = NULL;
  g_autofree gchar *url = NULL;
  g_autofree gchar *uniquieid = NULL;
  g_autofree gchar *title = NULL;
  g_autofree gchar *intro = NULL;
  g_autofree gchar *explanation = NULL;
  g_autofree gchar *annotation = NULL;
  g_autofree gchar *author = NULL;
  g_autofree gchar *editor = NULL;
  g_autofree gchar *date = NULL;
  g_autofree gchar *notes = NULL;
  g_autofree gchar *difficulty = NULL;
  g_autofree gchar *charset = NULL;
  g_autofree gchar *origin = NULL;
  g_autofree gchar *block = NULL;
  g_autofree gchar *empty = NULL;

  /* Crossword */
  gint width;
  gint height;
  gboolean showenumerations;
  IPuzCluePlacement clue_placement;
  IPuzBoard *board;
  IPuzCrosswordPrivate *priv;

  char ESC=27;

  g_object_get (G_OBJECT (self),
                "title", &title,
                "copyright", &copyright,
                "publisher", &publisher,
                "publication", &publication,
                "url", &url,
                "uniqueid", &uniquieid,
                "intro", &intro,
                "explanation", &explanation,
                "annotation", &annotation,
                "author", &author,
                "editor", &editor,
                "date", &date,
                "notes", &notes,
                "difficulty", &difficulty,
                "charset", &charset,
                "origin", &origin,
                "block", &block,
                "empty", &empty,
                "showenumerations", &showenumerations,
                "clue-placement", &clue_placement,
                "width", &width,
                "height", &height,
                "board", &board,
                NULL);
  priv = ipuz_crossword_get_instance_private (self);

  g_print ("\n");
  /* Print the title section */
  g_print ("%c[1mTitle: %s%c[0m\n", ESC, title?title:"(null)", ESC);

  /* Print the type */
  if (IPUZ_IS_ACROSTIC (self))
    g_print ("\tType: Acrostic Puzzle\n");
  else if (IPUZ_IS_ARROWWORD (self))
    g_print ("\tType: Arrowword Puzzle\n");
  else if (IPUZ_IS_BARRED (self))
    g_print ("\tType: Barred Puzzle\n");
  else if (IPUZ_IS_CRYPTIC (self))
    g_print ("\tType: Cryptic Crossword Puzzle\n");
  else if (IPUZ_IS_FILIPPINE (self))
    g_print ("\tType: Filippine Puzzle\n");
  else /* Default */
    g_print ("\tType: Crossword Puzzle\n");


  if (author || editor)
    {
      if (author) g_print ("\tby %s\t", author);
      if (editor) g_print ("\tedited by %s", editor);
      g_print ("\n");
    }
  if (copyright) g_print ("\tCopyright: %s\n", copyright);
  if (title || author || editor || copyright || date) g_print ("\n");

  for (int i = 0; i < width + 1; i++)
    g_print ("██");
  g_print ("\n");
  for (int row = 0; row < height; row ++)
    {
      g_print ("█");
      for (int column = 0; column < width; column ++)
        {
          IPuzCellCoord coord = { .row = row, .column = column };
          IPuzCell *cell = ipuz_board_get_cell (board, coord);
          gint n;
          switch (ipuz_cell_get_cell_type (cell))
            {
            case IPUZ_CELL_BLOCK:
              g_print ("▓▓");
              break;
            case IPUZ_CELL_NULL:
              g_print ("▞▚");
              break;
            case IPUZ_CELL_NORMAL:
              n = ipuz_cell_get_number (cell);
              if (n == 0)
                g_print ("  ");
              else
                g_print ((n<10?"%d ":"%d"),n);
              break;
            }
        }
      g_print ("█\n█");
      for (int column = 0; column < width; column ++)
        {
          const gchar *solution = NULL;
          IPuzCellCoord coord = { .row = row, .column = column };
          IPuzCell *cell = ipuz_board_get_cell (board, coord);
          switch (ipuz_cell_get_cell_type (cell))
            {
            case IPUZ_CELL_BLOCK:
              g_print ("▓▓");
              break;
            case IPUZ_CELL_NULL:
              g_print ("▚▞");
              break;
            case IPUZ_CELL_NORMAL:
               solution = ipuz_cell_get_solution (cell);
              if (solution)
                g_print (" %s", solution);
              else
                g_print ("  ");
              break;
            }
        }
      g_print ("█\n");
    }
  for (int column = 0; column < width + 1; column++)
    g_print ("██");
  g_print ("\n\n");

  GArray *clues;
  if (ipuz_crossword_get_n_clue_sets (self) > 0)
    g_print ("%c[1mClues%c[0m\n", ESC, ESC);
  for (guint i = 0; i < ipuz_crossword_get_n_clue_sets (self); i++)
    {
      IPuzClueDirection direction;
      IPuzClueDirection orig_direction;

      direction = ipuz_crossword_clue_set_get_dir (self, i);
      orig_direction = ipuz_clue_sets_get_original_direction (priv->clue_sets, direction);
      clues = ipuz_crossword_get_clues (self, direction);
      g_print ("\t%s:", ipuz_clue_direction_to_string (orig_direction));
      if (orig_direction != direction)
        g_print ("%s\n", ipuz_clue_sets_get_label (priv->clue_sets, direction));
      else
        g_print ("\n");
      print_clues (clues, showenumerations);
      g_print ("\n");
    }
  if (ipuz_crossword_get_n_clue_sets (self) > 0)
    g_print ("\n");

  g_print ("%c[1mDocument Information%c[0m\n", ESC, ESC);
  if (date) g_print ("\tDate: %s\n", date);
  if (publisher) g_print ("\tPublisher: %s\n", publisher);
  if (publication) g_print ("\tPublication: %s\n", publication);
  if (url) g_print ("\tURL: %s\n:", url);
  if (uniquieid) g_print ("\tUnique ID:%s\n", uniquieid);
  if (difficulty) g_print ("\tDifficulty:%s\n", difficulty);

  g_print ("\n");

  g_print ("%c[1mDisplay Information%c[0m\n", ESC, ESC);
  if (block) g_print ("\tBlock string: '%s'\n", block);
  if (empty) g_print ("\tEmpty cell string: '%s'\n", empty);
  if (charset) g_print ("\tValid charset: '%s'\n", charset);
  g_print ("\tShow enumerations: %s\n", showenumerations?"true":"false");

  switch (clue_placement)
    {
    case IPUZ_CLUE_PLACEMENT_NULL:
      g_print ("\tClue placement: null\n");
      break;
    case IPUZ_CLUE_PLACEMENT_BEFORE:
      g_print ("\tClue placement: before\n");
      break;
    case IPUZ_CLUE_PLACEMENT_AFTER:
      g_print ("\tClue placement: after\n");
      break;
    case IPUZ_CLUE_PLACEMENT_BLOCKS:
      g_print ("\tClue placement: blocks\n");
      break;
    }
  g_print ("\n");
#if 0
  "intro", &intro,
  "explanation", &explanation,
  "annotation", &annotation,
  "origin", &origin,
#endif

  g_object_unref (board);
}

/* ipuz-arrowword.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-crossword.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_ARROWWORD (ipuz_arrowword_get_type ())
G_DECLARE_DERIVABLE_TYPE   (IPuzArrowword, ipuz_arrowword, IPUZ, ARROWWORD, IPuzCrossword);


/* Determines the placement of clues in a cell
 */
typedef enum
{
  IPUZ_ARROWWORD_PLACEMENT_FILL,   /* @ Clue fills the cell @ */
  IPUZ_ARROWWORD_PLACEMENT_TOP,    /* @ Clue fills the top of the cell @ */
  IPUZ_ARROWWORD_PLACEMENT_BOTTOM, /* @ Clue fills the bottom of the cell @ */
} IPuzArrowwordPlacement;

/* Direction of the arrow coming out of the cell
 */
typedef enum
{
  IPUZ_ARROWWORD_ARROW_NONE,       /* @ Don't draw an arrow @ */
  IPUZ_ARROWWORD_ARROW_RIGHT,      /* @ → @ */
  IPUZ_ARROWWORD_ARROW_RIGHT_DOWN, /* @ ↴ @ */
  IPUZ_ARROWWORD_ARROW_DOWN,       /* @ ↓ @ */
  IPUZ_ARROWWORD_ARROW_DOWN_RIGHT, /* @ ↳ @ */
  IPUZ_ARROWWORD_ARROW_LEFT_DOWN,  /* @ ↙ @ */
  IPUZ_ARROWWORD_ARROW_UP_RIGHT,   /* @ ↱ @ */
} IPuzArrowwordArrow;


typedef void (*IPuzArrowwordForeachFunc) (IPuzArrowword          *arrowword,
                                          IPuzClue               *clue,
                                          IPuzCellCoord           block_coord,
                                          IPuzArrowwordPlacement  placement,
                                          IPuzArrowwordArrow      arrow,
                                          gpointer                user_data);

typedef struct _IPuzArrowwordClass
{
  IPuzCrosswordClass parent_class;
} IPuzArrowwordClass;


void ipuz_arrowword_blocks_foreach (IPuzArrowword            *self,
                                    IPuzArrowwordForeachFunc  func,
                                    gpointer                  data);
void ipuz_arrowword_print          (IPuzArrowword            *self);


G_END_DECLS

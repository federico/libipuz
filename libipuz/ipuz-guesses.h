/* ipuz-guesses.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-board.h>

G_BEGIN_DECLS


typedef struct _IPuzGuesses IPuzGuesses;


#define IPUZ_TYPE_GUESSES (ipuz_guesses_get_type ())


GType             ipuz_guesses_get_type        (void) G_GNUC_CONST;
IPuzGuesses      *ipuz_guesses_new_from_board  (IPuzBoard      *board,
                                                gboolean        copy_guesses);
IPuzGuesses      *ipuz_guesses_new_from_json   (JsonNode       *root,
                                                GError        **error);
IPuzGuesses      *ipuz_guesses_new_from_file   (const gchar    *filename,
                                                GError        **error);
IPuzGuesses      *ipuz_guesses_new_from_stream (GInputStream   *stream,
                                                GCancellable   *cancellable,
                                                GError        **error);
gboolean          ipuz_guesses_save_to_file    (IPuzGuesses    *guesses,
                                                const gchar    *filename,
                                                GError        **error);

IPuzGuesses      *ipuz_guesses_ref             (IPuzGuesses    *guesses);
void              ipuz_guesses_unref           (IPuzGuesses    *guesses);
IPuzGuesses      *ipuz_guesses_copy            (IPuzGuesses    *src);
gboolean          ipuz_guesses_equal           (IPuzGuesses    *a,
                                                IPuzGuesses    *b);

guint             ipuz_guesses_get_width       (IPuzGuesses    *guesses);
guint             ipuz_guesses_get_height      (IPuzGuesses    *guesses);
const gchar      *ipuz_guesses_get_guess       (IPuzGuesses    *guesses,
                                                IPuzCellCoord   coord);
void              ipuz_guesses_set_guess       (IPuzGuesses    *guesses,
                                                IPuzCellCoord   coord,
                                                const gchar    *guess);
IPuzCellCellType  ipuz_guesses_get_cell_type   (IPuzGuesses    *guesses,
                                                IPuzCellCoord   coord);
gfloat            ipuz_guesses_get_percent     (IPuzGuesses    *guesses);
gchar            *ipuz_guesses_get_checksum    (IPuzGuesses    *guesses,
                                                const gchar    *salt);


/* Public functions */
void              ipuz_guesses_print           (IPuzGuesses    *guesses);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(IPuzGuesses, ipuz_guesses_unref)


G_END_DECLS


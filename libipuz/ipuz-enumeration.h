/* ipuz-enumeration.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS


#define DELIM_IS_MODIFIER(d)  (d==IPUZ_DELIMINATOR_ALLCAPS||d==IPUZ_DELIMINATOR_CAPITALIZED||d ==IPUZ_DELIMINATOR_FOREIGN)
#define DELIM_IS_SEPARATOR(d) ((guint)d<=IPUZ_DELIMINATOR_APOSTROPHE)

typedef enum _IPuzDeliminator
{
  IPUZ_DELIMINATOR_WORD_BREAK,
  IPUZ_DELIMINATOR_PERIOD,
  IPUZ_DELIMINATOR_DASH,
  IPUZ_DELIMINATOR_APOSTROPHE,

  /* Word Modifiers */
  IPUZ_DELIMINATOR_ALLCAPS,
  IPUZ_DELIMINATOR_CAPITALIZED,
  IPUZ_DELIMINATOR_FOREIGN,
} IPuzDeliminator;

typedef enum _IPuzVerbosity
{
  IPUZ_VERBOSITY_STANDARD, /* "4,4" */
  IPUZ_VERBOSITY_SPARSE,   /* "Two words" */
} IPuzVerbosity;


/* grid_offset indicates what position within the clue the deliminator
 * can be found. It includes the borders within its count, as well as
 * the cells. For example, consider the enumeration "^4-4 3":
 *
 * |^| | | - | | | ǁ | | |
 *
 * We'll call this function with CAPITALIZED at position 1, DASH at
 * position 8, and WORD_BREAK at 16 and 22. final_word will be TRUE
 * when we get the WORD_BREAK at 22.
 *
 * Note, this last WORD_BREAK is implicit, and shouldn't be
 * rendered. It does help you know how long the final word is, though.
 */

typedef void (*IPuzDelimFunc) (IPuzDeliminator delim,
                               guint           grid_offset,
                               gboolean        final_word,
                               gpointer        user_data);

typedef struct _IPuzEnumeration IPuzEnumeration;

#define IPUZ_TYPE_ENUMERATION (ipuz_enumeration_get_type ())


GType            ipuz_enumeration_get_type      (void);
IPuzEnumeration *ipuz_enumeration_new           (const gchar           *src,
                                                 IPuzVerbosity          verbosity);
IPuzEnumeration *ipuz_enumeration_ref           (IPuzEnumeration       *enumeration);
void             ipuz_enumeration_unref         (IPuzEnumeration       *enumeration);
IPuzEnumeration *ipuz_enumeration_dup           (const IPuzEnumeration *enumeration);
gboolean         ipuz_enumeration_equal         (const IPuzEnumeration *enumeration1,
                                                 const IPuzEnumeration *enumeration2);
const gchar     *ipuz_enumeration_get_src       (const IPuzEnumeration *enumerationip);
const gchar     *ipuz_enumeration_get_display   (const IPuzEnumeration *enumeration);
gboolean         ipuz_enumeration_get_has_delim (const IPuzEnumeration *enumeration);
gint             ipuz_enumeration_delim_length  (const IPuzEnumeration *enumeration);
void             ipuz_enumeration_delim_foreach (const IPuzEnumeration *enumeration,
                                                 IPuzDelimFunc          func,
                                                 gpointer               user_data);
gboolean         ipuz_enumeration_valid_char    (gchar                  c);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(IPuzEnumeration, ipuz_enumeration_unref)


G_END_DECLS

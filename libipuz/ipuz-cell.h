/* ipuz-cell.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-clue.h>
#include <libipuz/ipuz-style.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_CELL (ipuz_cell_get_type ())
#define IPUZ_CELL_IS_NORMAL(cell)      ((cell) && (cell->cell_type == IPUZ_CELL_NORMAL))
#define IPUZ_CELL_IS_BLOCK(cell)       ((cell) && (cell->cell_type == IPUZ_CELL_BLOCK))
#define IPUZ_CELL_IS_NULL(cell)        ((cell) && (cell->cell_type == IPUZ_CELL_NULL))
#define IPUZ_CELL_IS_GUESSABLE(cell)   ((cell) && (cell->cell_type == IPUZ_CELL_NORMAL) && (cell->initial_val == NULL))
#define IPUZ_CELL_IS_INITIAL_VAL(cell) ((cell) && (cell->initial_val != NULL))

typedef enum
{
  IPUZ_CELL_NORMAL = 0, /* Regular cell */
  IPUZ_CELL_BLOCK,      /* Blocked out cell */
  IPUZ_CELL_NULL,       /* omitted cell */
} IPuzCellCellType;

/* This is safe to be allocated on the stack and used initialized to
   {0, } */
typedef struct _IPuzCell IPuzCell;
struct _IPuzCell
{
  IPuzCellCellType cell_type;
  gint number;
  gchar *label;
  gchar *solution;
  gchar *initial_val;

  /* This is the solved guesses loaded from file and to be saved
   * within the .ipuz file. It shouldn't be used to track the
   * transient state of a puzzle being solved; use IPuzGuesses instead
   * for that. */
  gchar *saved_guess;

  IPuzStyle *style;

  /* do not free */
  GArray *clues;

  /* Private */
  gchar *style_name;

};


GType            ipuz_cell_get_type             (void) G_GNUC_CONST;
IPuzCell        *ipuz_cell_new                  (void);
IPuzCell        *ipuz_cell_copy                 (const IPuzCell    *cell);
void             ipuz_cell_free                 (IPuzCell          *cell);
void             ipuz_cell_clear                (IPuzCell          *cell);
gboolean         ipuz_cell_equal                (IPuzCell          *a,
                                                 IPuzCell          *b);
void             ipuz_cell_build                (IPuzCell          *cell,
                                                 JsonBuilder       *builder,
                                                 gboolean           solution,
                                                 const char        *block,
                                                 const char        *empty);

IPuzCellCellType ipuz_cell_get_cell_type        (IPuzCell          *cell);
void             ipuz_cell_set_cell_type        (IPuzCell          *cell,
                                                 IPuzCellCellType   cell_type);
gint             ipuz_cell_get_number           (IPuzCell          *cell);
void             ipuz_cell_set_number           (IPuzCell          *cell,
                                                 gint               number);
const gchar     *ipuz_cell_get_label            (IPuzCell          *cell);
void             ipuz_cell_set_label            (IPuzCell          *cell,
                                                 const gchar       *label);
const gchar     *ipuz_cell_get_solution         (IPuzCell          *cell);
void             ipuz_cell_set_solution         (IPuzCell          *cell,
                                                 const gchar       *solution);
const gchar     *ipuz_cell_get_saved_guess      (IPuzCell          *cell);
void             ipuz_cell_set_saved_guess      (IPuzCell          *cell,
                                                 const gchar       *saved_guess);
const gchar     *ipuz_cell_get_initial_val      (IPuzCell          *cell);
void             ipuz_cell_set_initial_val      (IPuzCell          *cell,
                                                 const gchar       *initial_val);
IPuzStyle       *ipuz_cell_get_style            (IPuzCell          *cell);
void             ipuz_cell_set_style            (IPuzCell          *cell,
                                                 IPuzStyle         *style,
                                                 const gchar       *style_name);
void             ipuz_cell_set_clue             (IPuzCell          *cell,
                                                 IPuzClue          *clue);
void             ipuz_cell_clear_clue_direction (IPuzCell          *cell,
                                                 IPuzClueDirection  direction);
void             ipuz_cell_clear_clues          (IPuzCell          *cell);
const IPuzClue  *ipuz_cell_get_clue             (IPuzCell          *cell,
                                                 IPuzClueDirection  direction);

void             ipuz_cell_parse_puzzle         (IPuzCell          *cell,
                                                 JsonNode          *node,
                                                 const gchar       *block,
                                                 const gchar       *empty);
void             ipuz_cell_parse_solution       (IPuzCell          *cell,
                                                 JsonNode          *node,
                                                 const gchar       *block,
                                                 const gchar       *charset);


G_END_DECLS

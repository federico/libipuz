/* charset.h - Maintain a character set for a crossword puzzle
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_CHARSET (ipuz_charset_get_type ())
#define IPUZ_CHARSET(charset) ((IPuzCharset *)charset)

typedef struct _IPuzCharsetBuilder IPuzCharsetBuilder;
typedef struct _IPuzCharset IPuzCharset;
typedef struct _IPuzCharsetIter IPuzCharsetIter;


/* Keep in sync with rust/src/charset.c */
typedef struct _IPuzCharsetIterValue
{
  gunichar c;
  guint count;
} IPuzCharsetIterValue;


/**
 * ipuz_charset_builder_new:
 *
 * Returns: an empty builder for a character set.  Use `ipuz_charset_builder_add_text()` to populate it.
 */
IPuzCharsetBuilder   *ipuz_charset_builder_new              (void);

/**
 * ipuz_charset_builder_new_from_text:
 * @text: the text to base a new IPuzCharsetBuilder on, or NULL
 *
 * Returns: a new builder for character sets populated by @text.
 */
IPuzCharsetBuilder   *ipuz_charset_builder_new_from_text    (const char         *text);

/**
 * ipuz_charset_builder_new_for_language:
 * @lang: A language code, such as "en" or "es"
 *
 * Creates a charset builder with a list of all characters in common use in
 * crosswords for @lang's alphabet. @lang should be a country code,
 * but can be a fully-qualified locale (such as from the $LANG
 * environment variable). In that instance the remainder of the string
 * is ignored, as we don't consider regional distinctions when
 * determining a default alphabet. Along those lines, a lang of "C"
 * will return the English alphabet.
 *
 * Note that this returns the common alphabet of letters for a
 * language and will not include digraphs as independent
 * characters. As examples, Dutch will not include a separate 'ĳ'
 * digraph, despite the prevelance of "IJ" in Dutch puzzles.
 *
 * Returns: a newly allocated @IPuzCharsetBuilder, or NULL
 **/
IPuzCharsetBuilder   *ipuz_charset_builder_new_for_language (const char         *lang);

/**
 * ipuz_charset_builder_add_text:
 * @builder: the character set builder to populate.
 * @text: string with characters to add to the builder.
 *
 * Adds each unicode code point from @text into the @builder.
 */
void                  ipuz_charset_builder_add_text         (IPuzCharsetBuilder *builder,
                                                             const char         *text);

/**
 * ipuz_charset_builder_add_character:
 * @builder: the character set builder to extend.
 * @c: a unicode character to add to the builder.
 *
 * Adds @c to the @builder.
 **/
void                  ipuz_charset_builder_add_character    (IPuzCharsetBuilder *builder,
                                                             gunichar            c);

/**
 * ipuz_charset_builder_remove_text:
 * @builder: a character set builder with some characters in it.
 * @text: text whose characters should be tried to be removed.
 *
 * Tries to remove all the characters in @text from the @builder,
 * i.e. decrease its character counts by as many instances of each
 * character there are in @text.  If @text contains characters that
 * are not already in the @builder, or if @text contains more of a
 * certain character than @builder already has, this function returns #FALSE and leaves
 * the @builder unchanged.
 *
 * Returns: whether @builder had enough characters to remove all of @text's.  This
 * function only changes the contents @builder if it returns #TRUE; otherwise,
 * if it returns #FALSE, the @text could not be removed and @builder remains unchanged.
 */
gboolean              ipuz_charset_builder_remove_text      (IPuzCharsetBuilder *builder,
                                                             const char         *text);

/**
 * ipuz_charset_builder_build:
 * @builder: A character set builder that has already been populated.
 *
 * Consumes @builder and frees it, and returns an immutable
 * #IPuzCharset.  The resulting charset can be queried efficiently for
 * character counts and such.
 *
 * Returns: A charset compiled from the information in the @builder.  Use
 * ipuz_charset_unref() to free the return value.
 */
IPuzCharset          *ipuz_charset_builder_build            (IPuzCharsetBuilder *builder);

/**
 * ipuz_charset_ref:
 * @charset: the character set to ref
 *
 * Refs the character set.
 *
 * Returns: @charset. This can be used to chain calls or ref on return.
 */
IPuzCharset          *ipuz_charset_ref              (IPuzCharset       *charet);

/**
 * ipuz_charset_unref:
 * @charset: the character set to unref
 *
 * Unrefs a character set, which will be freed when the reference count becomes 0.
 */
void                  ipuz_charset_unref            (IPuzCharset       *charset);

/**
 * ipuz_charset_compare:
 * @charset_a: a charset
 * @charset_b: a charset
 *
 * Compares two charsets to see if they have exactly the same contents.
 *
 * Returns: #TRUE if the characters have the same characters and character counts.
 */
gboolean              ipuz_charset_compare          (IPuzCharset       *charset_a,
                                                     IPuzCharset       *charset_b);


/**
 * ipuz_charset_get_char_index:
 * @charset: the character set to search in.
 * @c: character to search for.
 *
 * Returns the index of the character @c in the @charset, or -1 if it does not exist.
 */
gint                  ipuz_charset_get_char_index   (const IPuzCharset *charset,
                                                     gunichar           c);

/**
 * ipuz_charset_get_char_count:
 * @charset: the character set to lookup in
 * @c: a character
 *
 *
 *
 * Returns: the number of instances of @c in the text
 **/
guint                 ipuz_charset_get_char_count   (const IPuzCharset *charset,
                                                     gunichar           c);

/**
 * ipuz_charset_get_n_chars:
 * @charset: the character set to query.
 *
 * Returns: the number of different types of characters stored in the
 * @charset. This is a constant-time operation.
 */
gsize                 ipuz_charset_get_n_chars      (const IPuzCharset *charset);

/**
 * ipuz_charset_get_size:
 * @charset: the character set to query
 *
 * Returns the total number of characters stored in @charset. Unlike
 * ipuz_charset_get_n_chars() which returns the number of types of
 * characters, this returns the count of characters.
 *
 * Returns: Total number of characters
 **/
gsize                 ipuz_charset_get_size         (const IPuzCharset *charset);

/**
 * ipuz_charset_iter_first:
 * @charset: A @IPuzCharset
 *
 * Gets an iteratior for querying the charset.  The first value can be queried
 * with ipuz_charset_iter_get_value().  Note that the iterator must be allowed to
 * terminate by running it until ipuz_charset_iter_next() returns #NULL; that last
 * iteration will free the #IPuzCharsetIter.
 *
 * Returns: The first iterator
 **/
IPuzCharsetIter      *ipuz_charset_iter_first       (const IPuzCharset *ipuz_charset);

/**
 * ipuz_charset_iter_next:
 * @iter: A @IPuzCharsetIter
 *
 * Returns the next iter in the charset sequentially. If we've reached
 * the end of the set, then return %NULL; this will free the #IPuzCharsetIter.
 *
 * Returns: the next @IPuzCharsetIter, or %NULL
 **/
IPuzCharsetIter      *ipuz_charset_iter_next        (IPuzCharsetIter   *iter);

/**
 * ipuz_charset_iter_get_value:
 * @iter: A @IPuzCharsetIter
 *
 * Returns: the current value of a character set iterator.  The value
 * contains the current character and its count.
 */
IPuzCharsetIterValue  ipuz_charset_iter_get_value   (IPuzCharsetIter   *iter);

/**
 * ipuz_charset_serialize:
 * @charset: the character set to serialize.
 *
 * Concatenates all the unique characters stored in a @charset, in the order in which they
 * would be returned by ipuz_charset_get_char_index().
 *
 * Returns: a string with all the characters from the character set.
 */
gchar                *ipuz_charset_serialize        (const IPuzCharset *ipuz_charset);

/**
 * ipuz_charset_deserialize:
 * @str: String serialization of a character set, as returned by ipuz_charset_serialize().
 *
 * Creates a new character set by deserializing from a string.
 *
 * Returns: a new character set.
 */
IPuzCharset          *ipuz_charset_deserialize      (const char        *str);


G_DEFINE_AUTOPTR_CLEANUP_FUNC (IPuzCharset, ipuz_charset_unref);


G_END_DECLS

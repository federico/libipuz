/* ipuz-puzzle.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */



#include "libipuz.h"
#include "libipuz-enums.h"
#include <json-glib/json-glib.h>
#include "libipuz-config.h"
#include <glib/gi18n-lib.h>
#include "ipuz-magic.h"
#include "ipuz-misc.h"
#include "ipuz-puzzle-info-private.h"


enum
{
  PROP_0,
  PROP_PUZZLE_KIND,
  PROP_VERSION,
  PROP_COPYRIGHT,
  PROP_PUBLISHER,
  PROP_PUBLICATION,
  PROP_URL,
  PROP_UNIQUEID,
  PROP_TITLE,
  PROP_INTRO,
  PROP_EXPLANATION,
  PROP_ANNOTATION,
  PROP_AUTHOR,
  PROP_EDITOR,
  PROP_DATE,
  PROP_NOTES,
  PROP_DIFFICULTY,
  PROP_CHARSET,
  PROP_ORIGIN,
  PROP_BLOCK,
  PROP_EMPTY,
  PROP_STYLES,
  PROP_LICENSE,
  PROP_LOCALE,
  N_PROPS
};
static GParamSpec *obj_props[N_PROPS] = { NULL, };

struct _IPuzPuzzlePrivate
{
  gchar *version;
  gchar *copyright;
  gchar *publisher;
  gchar *publication;
  gchar *url;
  gchar *uniqueid;
  gchar *title;
  gchar *intro;
  gchar *explanation;
  gchar *annotation;
  gchar *author;
  gchar *editor;
  gchar *date;
  gchar *notes;
  gchar *difficulty;
  gchar *charset;
  gchar *origin;
  gchar *block;
  gchar *empty;
  GHashTable *styles;

  /* Extenstions */
  gchar *license;
  gchar *locale;

  /* FIXME(checksum): add support for this */
  gchar *checksum_salt;
  gchar **checksums;
};


typedef struct _IPuzPuzzlePrivate IPuzPuzzlePrivate;


static void                ipuz_puzzle_init                (IPuzPuzzle      *self);
static void                ipuz_puzzle_class_init          (IPuzPuzzleClass *klass);
static void                ipuz_puzzle_dispose             (GObject         *object);
static void                ipuz_puzzle_finalize            (GObject         *object);
static void                ipuz_puzzle_set_property        (GObject         *object,
                                                            guint            prop_id,
                                                            const GValue    *value,
                                                            GParamSpec      *pspec);
static void                ipuz_puzzle_get_property        (GObject         *object,
                                                            guint            prop_id,
                                                            GValue          *value,
                                                            GParamSpec      *pspec);
static void                ipuz_puzzle_real_load_node      (IPuzPuzzle      *puzzle,
                                                            const char      *member_name,
                                                            JsonNode        *node);
static void                ipuz_puzzle_real_post_load_node (IPuzPuzzle      *puzzle,
                                                            const char      *member_name,
                                                            JsonNode        *node);
static void                ipuz_puzzle_real_fixup          (IPuzPuzzle      *puzzle);
static void                ipuz_puzzle_real_validate       (IPuzPuzzle      *puzzle);
static void                ipuz_puzzle_real_build          (IPuzPuzzle      *puzzle,
                                                            JsonBuilder     *builder);
static IPuzPuzzleFlags     ipuz_puzzle_real_get_flags      (IPuzPuzzle      *puzzle);
static void                ipuz_puzzle_real_calculate_info (IPuzPuzzle      *puzzle,
                                                            IPuzPuzzleInfo  *info);
static void                ipuz_puzzle_real_clone          (IPuzPuzzle      *src,
                                                            IPuzPuzzle      *dest);
static const char * const *ipuz_puzzle_real_get_kind_str   (IPuzPuzzle      *puzzle);
static void                ipuz_puzzle_real_set_style      (IPuzPuzzle      *puzzle,
                                                            const char      *style_name,
                                                            IPuzStyle       *style);
static gboolean            ipuz_puzzle_real_equal          (IPuzPuzzle      *puzzle_a,
                                                            IPuzPuzzle      *puzzle_b);


G_DEFINE_TYPE_WITH_CODE (IPuzPuzzle, ipuz_puzzle, G_TYPE_OBJECT, G_ADD_PRIVATE (IPuzPuzzle));


/*
 * Class Methods
 */

static void
ipuz_puzzle_init (IPuzPuzzle *puzzle)
{
  IPuzPuzzlePrivate *priv;

  priv = ipuz_puzzle_get_instance_private (puzzle);

  priv->version = g_strdup (_IPUZ_VERSION_2);
  priv->block = g_strdup (_IPUZ_DEFAULT_BLOCK);
  priv->empty = g_strdup (_IPUZ_DEFAULT_EMPTY);
}

static void
ipuz_puzzle_class_init (IPuzPuzzleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);

  object_class->finalize = ipuz_puzzle_finalize;
  object_class->dispose = ipuz_puzzle_dispose;
  object_class->set_property = ipuz_puzzle_set_property;
  object_class->get_property = ipuz_puzzle_get_property;
  klass->load_node = ipuz_puzzle_real_load_node;
  klass->post_load_node = ipuz_puzzle_real_post_load_node;
  klass->fixup = ipuz_puzzle_real_fixup;
  klass->validate = ipuz_puzzle_real_validate;
  klass->build = ipuz_puzzle_real_build;
  klass->get_flags = ipuz_puzzle_real_get_flags;
  klass->calculate_info = ipuz_puzzle_real_calculate_info;
  klass->clone = ipuz_puzzle_real_clone;
  klass->get_kind_str = ipuz_puzzle_real_get_kind_str;
  klass->set_style = ipuz_puzzle_real_set_style;
  klass->equal = ipuz_puzzle_real_equal;

  obj_props[PROP_PUZZLE_KIND] = g_param_spec_enum ("puzzle-kind",
						   "Puzzle Kind",
						   "The type of puzzle",
						   I_TYPE_PUZ_PUZZLE_KIND,
						   IPUZ_PUZZLE_UNKNOWN,
						   G_PARAM_READABLE);
  obj_props[PROP_VERSION] = g_param_spec_string ("version",
						 "Version",
						 "Version of ipuz for this puzzle",
						 _IPUZ_VERSION_2,
                                                 G_PARAM_READWRITE);
  obj_props[PROP_COPYRIGHT] = g_param_spec_string ("copyright",
						   "Copyright",
						   "Copyright information",
						   NULL,
						   G_PARAM_READWRITE);
  obj_props[PROP_PUBLISHER] = g_param_spec_string ("publisher",
						   "Publisher",
						   "Name and/or reference for a publisher",
						   NULL,
						   G_PARAM_READWRITE);
  obj_props[PROP_PUBLICATION] = g_param_spec_string ("publication",
						     "Publication",
						     "Bibliographic reference for a published puzzle",
						     NULL,
						     G_PARAM_READWRITE);
  obj_props[PROP_URL] = g_param_spec_string ("url",
					     "URL",
					     "Permanent URL for the puzzle",
					     NULL,
					     G_PARAM_READWRITE);
  obj_props[PROP_UNIQUEID] = g_param_spec_string ("uniqueid",
						  "Unique ID",
						  "Globally unique identifier for the puzzle",
						  NULL,
						  G_PARAM_READWRITE);
  obj_props[PROP_TITLE] = g_param_spec_string ("title",
					       "Title",
					       "Title of puzzle",
					       NULL,
					       G_PARAM_READWRITE);
  obj_props[PROP_INTRO] = g_param_spec_string ("intro",
					       "Intro",
					       "Text displayed above puzzle",
					       NULL,
					       G_PARAM_READWRITE);
  obj_props[PROP_EXPLANATION] = g_param_spec_string ("explanation",
						     "Explanation",
						     "Text displayed after successful solve",
						     NULL,
						     G_PARAM_READWRITE);
  obj_props[PROP_ANNOTATION] = g_param_spec_string ("annotation",
						    "Annotation",
						    "Non-displayed annotation",
						    NULL,
						    G_PARAM_READWRITE);
  obj_props[PROP_AUTHOR] = g_param_spec_string ("author",
						"Author",
						"Author of puzzle",
						NULL,
						G_PARAM_READWRITE);
  obj_props[PROP_EDITOR] = g_param_spec_string ("editor",
						"Editor",
						"Editor of puzzle",
						NULL,
						G_PARAM_READWRITE);
  obj_props[PROP_DATE] = g_param_spec_string ("date",
					      "Date",
					      "Date of puzzle or publication date",
					      NULL,
					      G_PARAM_READWRITE);
  obj_props[PROP_NOTES] = g_param_spec_string ("notes",
					       "Notes",
					       "Notes about the puzzle",
					       NULL,
					       G_PARAM_READWRITE);
  obj_props[PROP_DIFFICULTY] = g_param_spec_string ("difficulty",
						    "Difficulty",
						    "Informational only, there is no standard for difficulty",
						    NULL,
						    G_PARAM_READWRITE);
  obj_props[PROP_CHARSET] = g_param_spec_string ("charset",
						 "Charset",
						 "Characters that can be entered in the puzzle",
						 NULL,
						 G_PARAM_READWRITE);
  obj_props[PROP_ORIGIN] = g_param_spec_string ("origin",
						"Origin",
						"Program-specific information from program that wrote this file",
						NULL,
						G_PARAM_READWRITE);
  obj_props[PROP_BLOCK] = g_param_spec_string ("block",
					       "Block",
					       "Text value which represents a block",
					       _IPUZ_DEFAULT_BLOCK,
					       G_PARAM_READWRITE);
  obj_props[PROP_EMPTY] = g_param_spec_string ("empty",
					       "Empty",
					       "Value which represents an empty cell",
					       _IPUZ_DEFAULT_EMPTY,
					       G_PARAM_READWRITE);
  obj_props[PROP_STYLES] = g_param_spec_boxed ("styles",
                                               "Styles",
                                               "Named styles for the puzzle",
                                               G_TYPE_HASH_TABLE,
                                               G_PARAM_READWRITE);
  obj_props[PROP_LICENSE] = g_param_spec_string ("license",
                                                 "License",
                                                 "License of the puzzle",
                                                 NULL,
                                                 G_PARAM_READWRITE);
  obj_props[PROP_LOCALE] = g_param_spec_string ("locale",
                                                "Locale",
                                                "Locale of the puzzle",
                                                _IPUZ_DEFAULT_LOCALE,
                                                G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  /* I'm not sure where else to put this. */
  bindtextdomain (GETTEXT_PACKAGE, LIBIPUZ_LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

}


/* drop all refs */
static void
ipuz_puzzle_dispose (GObject *object)
{
  IPuzPuzzlePrivate *priv;

  priv = ipuz_puzzle_get_instance_private (IPUZ_PUZZLE (object));

  g_clear_pointer (&priv->styles, g_hash_table_unref);
  G_OBJECT_CLASS (ipuz_puzzle_parent_class)->dispose (object);
}

/* free all memory */
static void
ipuz_puzzle_finalize (GObject *object)
{
  IPuzPuzzlePrivate *priv;

  g_return_if_fail (object != NULL);

  priv = ipuz_puzzle_get_instance_private (IPUZ_PUZZLE (object));

  g_free (priv->version);
  g_free (priv->copyright);
  g_free (priv->publisher);
  g_free (priv->publication);
  g_free (priv->url);
  g_free (priv->uniqueid);
  g_free (priv->title);
  g_free (priv->intro);
  g_free (priv->explanation);
  g_free (priv->annotation);
  g_free (priv->author);
  g_free (priv->editor);
  g_free (priv->date);
  g_free (priv->notes);
  g_free (priv->difficulty);
  g_free (priv->charset);
  g_free (priv->origin);
  g_free (priv->block);
  g_free (priv->empty);
  g_free (priv->license);
  g_free (priv->locale);
  g_free (priv->checksum_salt);
  g_strfreev (priv->checksums);

  if (priv->styles)
    {
      g_hash_table_unref (priv->styles);
    }

  G_OBJECT_CLASS (ipuz_puzzle_parent_class)->finalize (object);
}


static void
ipuz_puzzle_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  IPuzPuzzle *self;
  IPuzPuzzlePrivate *priv;

  g_return_if_fail (object != NULL);

  self = IPUZ_PUZZLE (object);
  priv = ipuz_puzzle_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_VERSION:
      g_free (priv->version);
      priv->version = g_value_dup_string (value);
      break;
    case PROP_COPYRIGHT:
      g_free (priv->copyright);
      priv->copyright = g_value_dup_string (value);
      break;
    case PROP_PUBLISHER:
      g_free (priv->publisher);
      priv->publisher = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_PUBLICATION:
      g_free (priv->publication);
      priv->publication = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_URL:
      g_free (priv->url);
      priv->url = g_value_dup_string (value);
      break;
    case PROP_UNIQUEID:
      g_free (priv->uniqueid);
      priv->uniqueid = g_value_dup_string (value);
      break;
    case PROP_TITLE:
      g_free (priv->title);
      priv->title = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_INTRO:
      g_free (priv->intro);
      priv->intro = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_EXPLANATION:
      g_free (priv->explanation);
      priv->explanation = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_ANNOTATION:
      g_free (priv->annotation);
      priv->annotation = g_value_dup_string (value);
      break;
    case PROP_AUTHOR:
      g_free (priv->author);
      priv->author = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_EDITOR:
      g_free (priv->editor);
      priv->editor = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_DATE:
      g_free (priv->date);
      priv->date = g_value_dup_string (value);
      break;
    case PROP_NOTES:
      g_free (priv->notes);
      priv->notes = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_DIFFICULTY:
      g_free (priv->difficulty);
      priv->difficulty = ipuz_html_to_markup (g_value_get_string (value));
      break;
    case PROP_CHARSET:
      g_free (priv->charset);
      priv->charset = g_value_dup_string (value);
      break;
    case PROP_ORIGIN:
      g_free (priv->origin);
      priv->origin = g_value_dup_string (value);
      break;
    case PROP_BLOCK:
      g_free (priv->block);
      priv->block = g_value_dup_string (value);
      if (priv->block == NULL)
        priv->block = g_strdup (_IPUZ_DEFAULT_BLOCK);
      break;
    case PROP_EMPTY:
      g_free (priv->empty);
      priv->empty = g_value_dup_string(value);
      if (priv->empty == NULL)
        priv->empty = g_strdup (_IPUZ_DEFAULT_EMPTY);
      break;
    case PROP_STYLES:
      if (priv->styles)
        g_hash_table_unref (priv->styles);
      priv->styles = g_value_dup_boxed (value);
      break;
    case PROP_LICENSE:
      g_free (priv->license);
      priv->license = g_value_dup_string (value);
      break;
    case PROP_LOCALE:
      g_free (priv->locale);
      priv->locale = g_value_dup_string(value);
      if (priv->locale == NULL)
        priv->locale = g_strdup (_IPUZ_DEFAULT_LOCALE);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_puzzle_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  IPuzPuzzle *self;
  IPuzPuzzlePrivate *priv;

  g_return_if_fail (object != NULL);

  self = IPUZ_PUZZLE (object);
  priv = ipuz_puzzle_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_PUZZLE_KIND:
      g_value_set_enum (value, ipuz_puzzle_get_puzzle_kind (self));
      break;
    case PROP_VERSION:
      g_value_set_string (value, priv->version);
      break;
    case PROP_COPYRIGHT:
      g_value_set_string (value, priv->copyright);
      break;
    case PROP_PUBLISHER:
      g_value_set_string (value, priv->publisher);
      break;
    case PROP_PUBLICATION:
      g_value_set_string (value, priv->publication);
      break;
    case PROP_URL:
      g_value_set_string (value, priv->url);
      break;
    case PROP_UNIQUEID:
      g_value_set_string (value, priv->uniqueid);
      break;
    case PROP_TITLE:
      g_value_set_string (value, priv->title);
      break;
    case PROP_INTRO:
      g_value_set_string (value, priv->intro);
      break;
    case PROP_EXPLANATION:
      g_value_set_string (value, priv->explanation);
      break;
    case PROP_ANNOTATION:
      g_value_set_string (value, priv->annotation);
      break;
    case PROP_AUTHOR:
      g_value_set_string (value, priv->author);
      break;
    case PROP_EDITOR:
      g_value_set_string (value, priv->editor);
      break;
    case PROP_DATE:
      g_value_set_string (value, priv->date);
      break;
    case PROP_NOTES:
      g_value_set_string (value, priv->notes);
      break;
    case PROP_DIFFICULTY:
      g_value_set_string (value, priv->difficulty);
      break;
    case PROP_CHARSET:
      g_value_set_string (value, priv->charset);
      break;
    case PROP_ORIGIN:
      g_value_set_string (value, priv->origin);
      break;
    case PROP_BLOCK:
      if (priv->block)
        g_value_set_string (value, priv->block);
      else
        g_value_set_string (value, _IPUZ_DEFAULT_BLOCK);
      break;
    case PROP_EMPTY:
      if (priv->empty)
        g_value_set_string (value, priv->empty);
      else
        g_value_set_string (value, _IPUZ_DEFAULT_EMPTY);
      break;
    case PROP_STYLES:
      g_value_set_boxed (value, priv->styles);
      break;
    case PROP_LICENSE:
      g_value_set_string (value, priv->license);
      break;
    case PROP_LOCALE:
      g_value_set_string (value, priv->locale);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      break;
    }
}

static GHashTable *
new_styles_hash_table (void)
{
  return g_hash_table_new_full (g_str_hash, g_str_equal,
                                (GDestroyNotify) g_free,
                                (GDestroyNotify) ipuz_style_unref);
}

static void
ipuz_puzzle_real_load_styles (IPuzPuzzle *puzzle,
                              JsonNode   *node)
{
  IPuzPuzzlePrivate *priv;
  priv = ipuz_puzzle_get_instance_private (puzzle);
  JsonObjectIter iter = {0, };
  const gchar *member_name = NULL;
  JsonNode *member_node;

  if (!JSON_NODE_HOLDS_OBJECT (node))
    return;

  if (priv->styles == NULL)
    priv->styles = new_styles_hash_table ();

  json_object_iter_init (&iter, json_node_get_object (node));
  while (json_object_iter_next (&iter, &member_name, &member_node))
    {
      IPuzStyle *style = ipuz_style_new_from_json (member_node);
      ipuz_style_set_style_name (style, member_name);
      if (style != NULL)
        g_hash_table_insert (priv->styles, g_strdup (member_name), style);
    }
}

static void
ipuz_puzzle_real_load_node (IPuzPuzzle *puzzle,
                            const char *member_name,
                            JsonNode   *node)
{
  GObjectClass *object_class;
  GParamSpec *pspec;
  GValue value = G_VALUE_INIT;

  object_class = G_OBJECT_GET_CLASS (puzzle);

  /* styles isn't a simple property and needs manually loading */
  if (!g_strcmp0 (member_name, "styles"))
    {
      ipuz_puzzle_real_load_styles (puzzle, node);
      return;
    }

  /* Handle extensions separately */
  if (!g_strcmp0 (member_name, _IPUZ_X_LICENSE_TAG) ||
      !g_strcmp0 (member_name, _IPUZ_X_GNOME_LICENSE_TAG))
    {
      json_node_get_value (node, &value);
      g_object_set_property (G_OBJECT (puzzle), "license", &value);
      g_value_unset (&value);
      return;
    }

  if (!g_strcmp0 (member_name, _IPUZ_X_LOCALE_TAG) ||
      !g_strcmp0 (member_name, _IPUZ_X_GNOME_LOCALE_TAG))
    {
      json_node_get_value (node, &value);
      g_object_set_property (G_OBJECT (puzzle), "locale", &value);
      g_value_unset (&value);
      return;
    }
  /* handle a standard property */
  pspec = g_object_class_find_property (object_class, member_name);
  if (pspec == NULL)
    return;

  /* We don't do as comprehensive a job as the serializable interface, just
   * automatically handle strings and booleans. Everything else needs a custom
   * parser.
   */

  switch (G_PARAM_SPEC_VALUE_TYPE (pspec))
    {
    case G_TYPE_STRING:
      if (JSON_NODE_HOLDS_VALUE (node))
        {
          json_node_get_value (node, &value);
          g_object_set_property (G_OBJECT (puzzle), pspec->name, &value);
          g_value_unset (&value);
        }
      return;
    case G_TYPE_BOOLEAN:
      if (JSON_NODE_HOLDS_VALUE (node))
        {
          g_value_init (&value, G_TYPE_BOOLEAN);
          g_value_set_boolean (&value, json_node_get_boolean (node));
          g_object_set_property (G_OBJECT (puzzle), pspec->name, &value);
        }
      return;
    default:
      g_warning ("unable to convert %s", pspec->name);
      break;
    }
}

static void
ipuz_puzzle_real_post_load_node (IPuzPuzzle *puzzle,
                                 const char *member_name,
                                 JsonNode   *node)
{
  /* Empty function. Guard against accidentally being called by a subtype. */
}


static void
ipuz_puzzle_real_fixup (IPuzPuzzle *puzzle)
{
  /* Empty function. Guard against accidentally being called by a subtype. */
}

static void
ipuz_puzzle_real_validate (IPuzPuzzle *puzzle)
{
  /* FIXME: Implement someday */
}

static void
build_kind (IPuzPuzzle  *puzzle,
            JsonBuilder *builder)
{
  IPuzPuzzleClass *klass;
  const gchar *const *kind_str = NULL;

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);
  kind_str = klass->get_kind_str (puzzle);

  json_builder_set_member_name (builder, "kind");
  json_builder_begin_array (builder);

  if (kind_str == NULL)
    {
      json_builder_add_string_value (builder, "http://ipuz.org/crossword#1");
    }
  else
    {
      guint i = 0;

      while (kind_str[i] != NULL)
        {
          json_builder_add_string_value (builder, kind_str[i]);
          i++;
        }
    }
  json_builder_end_array (builder);
}

static void
build_string_param (IPuzPuzzle  *puzzle,
                    GParamSpec  *pspec,
                    JsonBuilder *builder)
{
  const gchar *name;
  GValue value = G_VALUE_INIT;

  name = g_param_spec_get_name (pspec);

  g_value_init (&value, G_TYPE_STRING);

  g_object_get_property (G_OBJECT (puzzle), name, &value);
  if (g_value_get_string (&value) != NULL)
    {
      if (!g_strcmp0 (name, "license"))
        name = _IPUZ_X_LICENSE_TAG;
      else if (!g_strcmp0 (name, "locale"))
        name = _IPUZ_X_LOCALE_TAG;

      json_builder_set_member_name (builder, name);
      json_builder_add_string_value (builder, g_value_get_string (&value));
    }
  g_value_unset (&value);
}


static void
build_styles_foreach (const char  *style_name,
                      IPuzStyle   *style,
                      JsonBuilder *builder)
{
  g_return_if_fail (style_name != NULL);
  g_return_if_fail (style != NULL);

  json_builder_set_member_name (builder, style_name);
  ipuz_style_build (style, builder);
}

static void
build_styles (IPuzPuzzle  *puzzle,
              JsonBuilder *builder)
{
  IPuzPuzzlePrivate *priv;

  priv = ipuz_puzzle_get_instance_private (puzzle);
  if (priv->styles == NULL)
    return;

  json_builder_set_member_name (builder, "styles");
  json_builder_begin_object (builder);
  g_hash_table_foreach (priv->styles, (GHFunc) build_styles_foreach, builder);
  json_builder_end_object (builder);

}
static void
ipuz_puzzle_real_build (IPuzPuzzle  *puzzle,
                        JsonBuilder *builder)
{
  /* Print the version and kind */
  build_kind (puzzle, builder);

  /* Print the string properties */
  guint i;
  for (i = PROP_0 + 1; i < N_PROPS; i++)
    {
      if (G_PARAM_SPEC_VALUE_TYPE (obj_props[i]) == G_TYPE_STRING)
        build_string_param (puzzle, obj_props[i], builder);
    }

  /* Print puzzle-wide styles */
  build_styles (puzzle, builder);

  /* FIXME(checksum): write this */
  /* build_checksum (puzzle, builder); */
}

static IPuzPuzzleFlags
ipuz_puzzle_real_get_flags (IPuzPuzzle *puzzle)
{
  IPuzPuzzlePrivate *priv;
  guint flags = 0;

  priv = ipuz_puzzle_get_instance_private (puzzle);

  if (priv->checksums != NULL)
    flags |= IPUZ_PUZZLE_FLAG_HAS_CHECKSUM;

  return flags;
}

static void
ipuz_puzzle_real_calculate_info (IPuzPuzzle      *puzzle,
                                 IPuzPuzzleInfo  *info)
{
  IPuzPuzzlePrivate *priv;

  g_assert (IPUZ_IS_PUZZLE (puzzle));
  g_assert (IPUZ_IS_PUZZLE_INFO (info));

  priv = ipuz_puzzle_get_instance_private (puzzle);

  info->charset = ipuz_puzzle_get_charset (puzzle);

  if (priv->checksums != NULL)
    info->flags |= IPUZ_PUZZLE_FLAG_HAS_CHECKSUM;
}

static void
styles_copy_func (const gchar *key,
                  IPuzStyle   *style,
                  GHashTable  *dest_styles)
{
  IPuzStyle *new_style;

  new_style = ipuz_style_copy (style);
  g_hash_table_insert (dest_styles, g_strdup (key), new_style);
}

static void
ipuz_puzzle_real_clone (IPuzPuzzle *src,
                        IPuzPuzzle *dest)
{
  IPuzPuzzlePrivate *src_priv, *dest_priv;

  g_return_if_fail (dest != NULL);

  src_priv = ipuz_puzzle_get_instance_private (src);
  dest_priv = ipuz_puzzle_get_instance_private (dest);

  g_clear_pointer (&dest_priv->version, g_free);
  dest_priv->version = g_strdup (src_priv->version);

  g_clear_pointer (&dest_priv->copyright, g_free);
  dest_priv->copyright = g_strdup (src_priv->copyright);

  g_clear_pointer (&dest_priv->publisher, g_free);
  dest_priv->publisher = g_strdup (src_priv->publisher);

  g_clear_pointer (&dest_priv->publication, g_free);
  dest_priv->publication = g_strdup (src_priv->publication);

  g_clear_pointer (&dest_priv->url, g_free);
  dest_priv->url = g_strdup (src_priv->url);

  g_clear_pointer (&dest_priv->uniqueid, g_free);
  dest_priv->uniqueid = g_strdup (src_priv->uniqueid);

  g_clear_pointer (&dest_priv->title, g_free);
  dest_priv->title = g_strdup (src_priv->title);

  g_clear_pointer (&dest_priv->intro, g_free);
  dest_priv->intro = g_strdup (src_priv->intro);

  g_clear_pointer (&dest_priv->explanation, g_free);
  dest_priv->explanation = g_strdup (src_priv->explanation);

  g_clear_pointer (&dest_priv->annotation, g_free);
  dest_priv->annotation = g_strdup (src_priv->annotation);

  g_clear_pointer (&dest_priv->author, g_free);
  dest_priv->author = g_strdup (src_priv->author);

  g_clear_pointer (&dest_priv->editor, g_free);
  dest_priv->editor = g_strdup (src_priv->editor);

  g_clear_pointer (&dest_priv->date, g_free);
  dest_priv->date = g_strdup (src_priv->date);

  g_clear_pointer (&dest_priv->notes, g_free);
  dest_priv->notes = g_strdup (src_priv->notes);

  g_clear_pointer (&dest_priv->difficulty, g_free);
  dest_priv->difficulty = g_strdup (src_priv->difficulty);

  g_clear_pointer (&dest_priv->charset, g_free);
  dest_priv->charset = g_strdup (src_priv->charset);

  g_clear_pointer (&dest_priv->origin, g_free);
  dest_priv->origin = g_strdup (src_priv->origin);

  g_clear_pointer (&dest_priv->block, g_free);
  dest_priv->block = g_strdup (src_priv->block);

  g_clear_pointer (&dest_priv->empty, g_free);
  dest_priv->empty = g_strdup (src_priv->empty);

  g_clear_pointer (&dest_priv->styles, g_hash_table_unref);
  if (src_priv->styles)
    {
      dest_priv->styles = new_styles_hash_table ();
      g_hash_table_foreach (src_priv->styles, (GHFunc) styles_copy_func, dest_priv->styles);
    }

  g_clear_pointer (&dest_priv->license, g_free);
  dest_priv->license = g_strdup (src_priv->license);

  g_clear_pointer (&dest_priv->locale, g_free);
  dest_priv->locale = g_strdup (src_priv->locale);

  g_clear_pointer (&dest_priv->checksum_salt, g_free);
  dest_priv->checksum_salt = g_strdup (src_priv->checksum_salt);

  g_clear_pointer (&dest_priv->checksums, g_strfreev);
  dest_priv->checksums = g_strdupv (src_priv->checksums);
}

static const char * const *
ipuz_puzzle_real_get_kind_str (IPuzPuzzle *puzzle)
{
  return NULL;
}

static void
ipuz_puzzle_real_set_style (IPuzPuzzle *puzzle,
                            const char *style_name,
                            IPuzStyle  *style)
{
  IPuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (puzzle));

  priv = ipuz_puzzle_get_instance_private (puzzle);

  if (priv->styles == NULL)
    priv->styles = new_styles_hash_table ();

  if (style == NULL)
    g_hash_table_remove (priv->styles, style_name);
  else
    g_hash_table_replace (priv->styles, g_strdup (style_name), ipuz_style_ref (style));
}

static gboolean
ipuz_puzzle_real_equal (IPuzPuzzle *puzzle_a,
                        IPuzPuzzle *puzzle_b)
{
  IPuzPuzzlePrivate *priv_a, *priv_b;
  priv_a = ipuz_puzzle_get_instance_private (puzzle_a);
  priv_b = ipuz_puzzle_get_instance_private (puzzle_b);

  if (priv_a->checksums)
    {
      if (priv_b->checksums == NULL)
        return FALSE;

      for (int i = 0; priv_a->checksums[i] != NULL; i++)
        {
          if (g_strcmp0 (priv_a->checksums[i], priv_b->checksums[i]) != 0)
            return FALSE;
        }
    }
  else if (priv_b->checksums)
    return FALSE;

  if (priv_a->styles)
    {
      if (priv_b->styles == NULL)
        return FALSE;

      GHashTableIter iter;
      gpointer key, value;

      g_hash_table_iter_init (&iter, priv_a->styles);

      while (g_hash_table_iter_next (&iter, &key, &value))
        {
          if (! ipuz_style_equal (g_hash_table_lookup (priv_b->styles, key), value))
            return FALSE;
        }
    }
  else if (priv_b->styles)
    return FALSE;

  return (!g_strcmp0 (priv_a->version, priv_b->version)
          && !g_strcmp0 (priv_a->copyright, priv_b->copyright)
          && !g_strcmp0 (priv_a->publisher, priv_b->publisher)
          && !g_strcmp0 (priv_a->publication, priv_b->publication)
          && !g_strcmp0 (priv_a->url, priv_b->url)
          && !g_strcmp0 (priv_a->uniqueid, priv_b->uniqueid)
          && !g_strcmp0 (priv_a->title, priv_b->title)
          && !g_strcmp0 (priv_a->intro, priv_b->intro)
          && !g_strcmp0 (priv_a->explanation, priv_b->explanation)
          && !g_strcmp0 (priv_a->annotation, priv_b->annotation)
          && !g_strcmp0 (priv_a->author, priv_b->author)
          && !g_strcmp0 (priv_a->editor, priv_b->editor)
          && !g_strcmp0 (priv_a->date, priv_b->date)
          && !g_strcmp0 (priv_a->notes, priv_b->notes)
          && !g_strcmp0 (priv_a->difficulty, priv_b->difficulty)
          && !g_strcmp0 (priv_a->charset, priv_b->charset)
          && !g_strcmp0 (priv_a->origin, priv_b->origin)
          && !g_strcmp0 (priv_a->block, priv_b->block)
          && !g_strcmp0 (priv_a->empty, priv_b->empty)
          && !g_strcmp0 (priv_a->license, priv_b->license)
          && !g_strcmp0 (priv_a->locale, priv_b->locale)
          && !g_strcmp0 (priv_a->checksum_salt, priv_b->checksum_salt));
}

/*
 * Helper functions
 */

/**
 * Confirm that the version is one we can support.
 */
static const gchar *
ipuz_puzzle_valid_puzzle_version (JsonNode  *root,
                                  GError   **error)
{
  g_return_val_if_fail (error !=NULL && *error == NULL, NULL);

  g_autoptr (JsonPath) path = json_path_new ();
  json_path_compile (path, "$.version", NULL);
  g_autoptr (JsonNode) result = json_path_match (path, root);

  if (result == NULL)
    {
      *error = g_error_new (IPUZ_ERROR, IPUZ_ERROR_INVALID_FILE, _("Missing version tag."));
      return NULL;
    }

  JsonArray *arr = json_node_get_array (result);
  JsonNode *element = json_array_get_element (arr, 0);
  const gchar *str = json_node_get_string (element);

  /* I don't know what version 1 really is, but we see them in the
   * wild occasionally and they seem to load fine */
  if (g_strcmp0 (str, _IPUZ_VERSION_1) == 0)
    return _IPUZ_VERSION_1;
  else if (g_strcmp0 (str, _IPUZ_VERSION_2) == 0)
    return _IPUZ_VERSION_2;

  *error = g_error_new (IPUZ_ERROR, IPUZ_ERROR_WRONG_VERSION, _("Unhandled version: %s"), str);
  return NULL;
}

gboolean
check_kind_version (const gchar *str,
                    const gchar *prefix,
                    gint         version)
{
  size_t len;

  g_return_val_if_fail (str != NULL, FALSE);
  g_return_val_if_fail (prefix != NULL, FALSE);

  len = strlen (prefix);
  if (strncmp (str, prefix, len) == 0)
    {
      if (str[len] == '#') /* Check for an optional version */
        {
          guint64 puz_version = g_ascii_strtoull (str + (len + 1), NULL, 10);
          if ((gint) puz_version <= version)
            return TRUE;
        }
      else if (! str[len]) /* We support unversioned puzzles */
        return TRUE;
    }

  return FALSE;
}

/**
 * Figure out what kind of puzzle
 */
static IPuzPuzzleKind
ipuz_puzzle_parse_kind (JsonNode  *root,
                        GError   **error)
{
  g_autoptr (JsonPath) path = NULL;
  g_autoptr (JsonNode) result = NULL;
  JsonArray *array;
  IPuzPuzzleKind kind = IPUZ_PUZZLE_UNKNOWN;

  /* We use json path as we're not ready to walk the tree yet */
  path = json_path_new ();
  json_path_compile (path, "$.kind[*]", NULL);
  result = json_path_match (path, root);

  if (result == NULL)
    {
      *error = g_error_new (IPUZ_ERROR, IPUZ_ERROR_INVALID_FILE, _("Missing the kind tag. This doesn't look like an ipuz file."));
      return IPUZ_PUZZLE_UNKNOWN;
    }

  array = json_node_get_array (result);
  for (guint i = 0; i < json_array_get_length (array); i++)
    {
      JsonNode *element;
      const gchar *str;

      element = json_array_get_element (array, i);
      if (!JSON_NODE_HOLDS_VALUE (element))
        continue;
      str = json_node_get_string (element);
      if (str == NULL)
        continue;

      if (check_kind_version (str, _IPUZ_ARROWWORD_PREFIX, _IPUZ_ARROWWORD_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_ARROWWORD;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_BARRED_PREFIX, _IPUZ_BARRED_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_BARRED;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_FILIPPINE_PREFIX, _IPUZ_FILIPPINE_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_FILIPPINE;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_CRYPTIC_PREFIX, _IPUZ_CRYPTIC_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_CRYPTIC;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_ACROSTIC_PREFIX, _IPUZ_ACROSTIC_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
	      kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_ACROSTIC;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_CROSSWORD_PREFIX, _IPUZ_CROSSWORD_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN)
            kind = IPUZ_PUZZLE_CROSSWORD;
          continue;
        }
    }
  return kind;
}


static void
ipuz_puzzle_new_foreach (JsonObject  *object,
                         const gchar *member_name,
                         JsonNode    *member_node,
                         IPuzPuzzle  *puzzle)
{
  IPuzPuzzleClass *klass;

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);
  g_return_if_fail (klass->load_node != NULL);

  klass->load_node (puzzle, member_name, member_node);
}

static void
ipuz_puzzle_new_foreach_post (JsonObject  *object,
                              const gchar *member_name,
                              JsonNode    *member_node,
                              IPuzPuzzle  *puzzle)
{
  IPuzPuzzleClass *klass;

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);

  if (klass->post_load_node)
    klass->post_load_node (puzzle, member_name, member_node);
}

IPuzPuzzle *
ipuz_puzzle_new_from_json (JsonNode  *root,
                           GError   **error)
{
  GError *tmp_error = NULL;
  IPuzPuzzle *puzzle = NULL;
  IPuzPuzzleKind kind;
  const gchar *ipuz_version;

  g_return_val_if_fail (root != NULL, NULL);

  if (!JSON_NODE_HOLDS_OBJECT (root))
    {
      if (error)
        *error = g_error_new (IPUZ_ERROR, IPUZ_ERROR_INVALID_FILE, "The first element isn't an object");
      return NULL;
    }

  ipuz_version = ipuz_puzzle_valid_puzzle_version (root, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  kind = ipuz_puzzle_parse_kind (root, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  if (kind == IPUZ_PUZZLE_UNKNOWN)
    {
      if (error)
        *error = g_error_new (IPUZ_ERROR, IPUZ_ERROR_INVALID_FILE, "Unknown puzzle type");
      return NULL;
    }

  switch (kind)
    {
    case IPUZ_PUZZLE_CROSSWORD:
      puzzle = g_object_new (IPUZ_TYPE_CROSSWORD,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_ARROWWORD:
      puzzle = g_object_new (IPUZ_TYPE_ARROWWORD,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_BARRED:
      puzzle = g_object_new (IPUZ_TYPE_BARRED,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_CRYPTIC:
      puzzle = g_object_new (IPUZ_TYPE_CRYPTIC,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_FILIPPINE:
      puzzle = g_object_new (IPUZ_TYPE_FILIPPINE,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_ACROSTIC:
      puzzle = g_object_new (IPUZ_TYPE_ACROSTIC,
                             "version", ipuz_version,
                             NULL);
      break;
    default:
      g_assert_not_reached ();
    }

  /* Load the puzzle as best as we can */
  IPuzPuzzleClass *klass = IPUZ_PUZZLE_GET_CLASS (puzzle);
  JsonObject *object = json_node_get_object (root);
  g_object_freeze_notify (G_OBJECT (puzzle));

  /* We do a multi-pass parse. See parsing-strategy.md for more information */
  json_object_foreach_member (object, (JsonObjectForeach) ipuz_puzzle_new_foreach, puzzle);
  json_object_foreach_member (object, (JsonObjectForeach) ipuz_puzzle_new_foreach_post, puzzle);
  klass->fixup (puzzle);
  klass->validate (puzzle);

  g_object_thaw_notify (G_OBJECT (puzzle));

  return puzzle;
}

IPuzPuzzle *
ipuz_puzzle_new_from_file (const char  *filename,
                           GError     **error)
{
  GError *tmp_error = NULL;
  g_autoptr(JsonParser) parser = NULL;

  g_return_val_if_fail (filename != NULL, NULL);

  parser = json_parser_new ();
  json_parser_load_from_file (parser, filename, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  return ipuz_puzzle_new_from_json (json_parser_get_root (parser), error);
}

IPuzPuzzle *
ipuz_puzzle_new_from_stream (GInputStream  *stream,
                             GCancellable  *cancellable,
                             GError       **error)
{
  GError *tmp_error = NULL;
  g_autoptr(JsonParser) parser = NULL;

  g_return_val_if_fail (G_IS_INPUT_STREAM (stream), NULL);

  parser = json_parser_new ();
  json_parser_load_from_stream (parser, stream, cancellable, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  return ipuz_puzzle_new_from_json (json_parser_get_root (parser), error);
}

IPuzPuzzle *
ipuz_puzzle_new_from_data (const gchar   *data,
                           gsize          length,
                           GError       **error)
{
  GError *tmp_error = NULL;
  g_autoptr(JsonParser) parser = NULL;

  g_return_val_if_fail (data != NULL, NULL);

  parser = json_parser_new ();
  json_parser_load_from_data (parser, data, length, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  return ipuz_puzzle_new_from_json (json_parser_get_root (parser), error);
}



static JsonGenerator *
ipuz_puzzle_get_generator (IPuzPuzzle *puzzle)
{
  g_autoptr (JsonBuilder) builder = NULL;
  JsonGenerator *generator;
  IPuzPuzzleClass *klass;
  g_autoptr (JsonNode) root = NULL;


  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);

  builder = json_builder_new ();
  json_builder_begin_object (builder);

  generator = json_generator_new ();
  json_generator_set_pretty (generator, TRUE);

  if (klass->build)
    klass->build (puzzle, builder);

  json_builder_end_object (builder);
  root = json_builder_get_root (builder);
  json_generator_set_root (generator, root);

  return generator;
}


gboolean
ipuz_puzzle_save_to_file (IPuzPuzzle  *puzzle,
                          const char  *filename,
                          GError     **error)
{
  g_autoptr (JsonGenerator) generator = NULL;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), FALSE);

  generator = ipuz_puzzle_get_generator (puzzle);
  return json_generator_to_file (generator, filename, error);
}

gboolean
ipuz_puzzle_save_to_stream (IPuzPuzzle     *puzzle,
                            GOutputStream  *stream,
                            GCancellable   *cancellable,
                            GError        **error)
{
  g_autoptr (JsonGenerator) generator = NULL;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), FALSE);
  g_return_val_if_fail (G_IS_OUTPUT_STREAM (stream), FALSE);

  generator = ipuz_puzzle_get_generator (puzzle);
  return json_generator_to_stream (generator, stream, cancellable, error);

}

gchar *
ipuz_puzzle_save_to_data (IPuzPuzzle *puzzle,
                          gsize      *length)
{
  g_autoptr (JsonGenerator) generator = NULL;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), NULL);

  generator = ipuz_puzzle_get_generator (puzzle);

  return json_generator_to_data (generator, length);
}

/**
 * ipuz_puzzle_get_puzzle_kind:
 * @self: An #IPuzPuzzle
 *
 * Returns the type of puzzle of @self. This can be more useful than
 * the GType for switch statements or other conditionals.
 *
 * Note, this only returns the more specific type. So a cryptic
 * crossword will return #IPUZ_PUZZLE_CRYPTIC and not
 * #IPUZ_PUZZLE_CROSSWORD.
 *
 * Returns: The puzzle kind
 **/
IPuzPuzzleKind
ipuz_puzzle_get_puzzle_kind (IPuzPuzzle *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), IPUZ_PUZZLE_UNKNOWN);

  if (IPUZ_IS_ACROSTIC (self))
    return IPUZ_PUZZLE_ACROSTIC;
  else if (IPUZ_IS_ARROWWORD (self))
    return IPUZ_PUZZLE_ARROWWORD;
  else if (IPUZ_IS_BARRED (self))
    return IPUZ_PUZZLE_BARRED;
  else if (IPUZ_IS_CRYPTIC (self))
    return IPUZ_PUZZLE_CRYPTIC;
  else if (IPUZ_IS_FILIPPINE (self))
    return IPUZ_PUZZLE_FILIPPINE;
  /* Crossword has to be last otherwise the ones that inherit from it
   * will trigger this boolean */
  else if (IPUZ_IS_CROSSWORD (self))
    return IPUZ_PUZZLE_CROSSWORD;

  return IPUZ_PUZZLE_UNKNOWN;
}

/**
 * ipuz_puzzle_get_flags:
 * @puzzle: An `IPuzPuzzle`
 *
 * Indicates static properties about the puzzle at load time.
 *
 * Returns: flags, with properties about the puzzle
 **/
IPuzPuzzleFlags
ipuz_puzzle_get_flags (IPuzPuzzle *puzzle)
{
  IPuzPuzzleClass *klass;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), 0);

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);

  return klass->get_flags (puzzle);
}

/**
 * ipuz_puzzle_get_info:
 * @puzzle: An `IPuzPuzzle`
 *
 * Calculates information about the current state of @puzzle. This
 * information is not kept in sync with the puzzle, so if there are
 * any changes to it it will have to be recalculated.
 *
 * Returns: a newly allocated `IPuzPuzzleInfo` object
 **/
IPuzPuzzleInfo *
ipuz_puzzle_get_info (IPuzPuzzle *puzzle)
{
  IPuzPuzzleClass *klass;
  IPuzPuzzleInfo *info;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), 0);

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);
  info = g_object_new (IPUZ_TYPE_PUZZLE_INFO, NULL);

  klass->calculate_info (puzzle, info);

  return info;
}

IPuzPuzzle *
ipuz_puzzle_deep_copy (IPuzPuzzle *src)
{
  IPuzPuzzle *dest;
  IPuzPuzzleClass *klass;

  /* We allow NULL puzzles to return */
  if (src == NULL)
    return NULL;
  g_return_val_if_fail (IPUZ_IS_PUZZLE (src), NULL);

  dest = g_object_new (G_TYPE_FROM_INSTANCE (src), NULL);

  klass = IPUZ_PUZZLE_GET_CLASS (src);
  klass->clone (src, dest);

  return dest;
}

gboolean
ipuz_puzzle_equal (IPuzPuzzle *puzzle_a,
                   IPuzPuzzle *puzzle_b)
{
  IPuzPuzzleClass *klass;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle_a), FALSE);
  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle_b), FALSE);

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle_a);

  return klass->equal (puzzle_a, puzzle_b);
}

/* Getters and Setters */

IPuzCharset *
ipuz_puzzle_get_charset (IPuzPuzzle *self)
{
  IPuzPuzzlePrivate *priv;
  IPuzCharsetBuilder *builder;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  if (priv->charset)
    builder = ipuz_charset_builder_new_from_text (priv->charset);
  else if (priv->locale)
    builder = ipuz_charset_builder_new_for_language (priv->locale);
  else
    builder = ipuz_charset_builder_new_for_language ("C");

  return ipuz_charset_builder_build (builder);
}

IPuzStyle *
ipuz_puzzle_get_style (IPuzPuzzle  *self,
                       const gchar *style_name)
{
  IPuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  if (priv->styles)
    return (IPuzStyle *) g_hash_table_lookup (priv->styles, style_name);

  return NULL;
}

void
ipuz_puzzle_set_style (IPuzPuzzle  *self,
                       const gchar *style_name,
                       IPuzStyle   *style)
{
  IPuzPuzzleClass *klass;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  klass = IPUZ_PUZZLE_GET_CLASS (self);

  klass->set_style (self, style_name, style);
}

void
ipuz_puzzle_style_foreach (IPuzPuzzle           *self,
                           IPuzStyleForeachFunc  func,
                           gpointer              user_data)
{
  IPuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  if (priv->styles)
    g_hash_table_foreach (priv->styles, (GHFunc) func, user_data);
}

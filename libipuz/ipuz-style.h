/* ipuz-style.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_STYLE (ipuz_style_get_type ())
#define IPUZ_STYLE(style) ((IPuzStyle *)style)
typedef enum
{
  IPUZ_STYLE_SHAPE_NONE,
  IPUZ_STYLE_SHAPE_CIRCLE,
  IPUZ_STYLE_SHAPE_ARROW_LEFT,
  IPUZ_STYLE_SHAPE_ARROW_RIGHT,
  IPUZ_STYLE_SHAPE_ARROW_UP,
  IPUZ_STYLE_SHAPE_ARROW_DOWN,
  IPUZ_STYLE_SHAPE_TRIANGLE_LEFT,
  IPUZ_STYLE_SHAPE_TRIANGLE_RIGHT,
  IPUZ_STYLE_SHAPE_TRIANGLE_UP,
  IPUZ_STYLE_SHAPE_TRIANGLE_DOWN,
  IPUZ_STYLE_SHAPE_DIAMOND,
  IPUZ_STYLE_SHAPE_CLUB,
  IPUZ_STYLE_SHAPE_HEART,
  IPUZ_STYLE_SHAPE_SPADE,
  IPUZ_STYLE_SHAPE_STAR,
  IPUZ_STYLE_SHAPE_SQUARE,
  IPUZ_STYLE_SHAPE_RHOMBUS,
  IPUZ_STYLE_SHAPE_SLASH,
  IPUZ_STYLE_SHAPE_BACKSLASH,
  IPUZ_STYLE_SHAPE_X,
} IPuzStyleShape;

typedef enum
{
  IPUZ_STYLE_DIVIDED_NONE,
  IPUZ_STYLE_DIVIDED_HORIZ,    /* @ "-" @ */
  IPUZ_STYLE_DIVIDED_VERT,     /* @ "|" @ */
  IPUZ_STYLE_DIVIDED_UP_RIGHT, /* @ "/" @ */
  IPUZ_STYLE_DIVIDED_UP_LEFT,  /* @ "\" @ */
  IPUZ_STYLE_DIVIDED_PLUS,     /* @ "+" @ */
  IPUZ_STYLE_DIVIDED_CROSS,    /* @ "X" @ */
} IPuzStyleDivided;

typedef enum
{
  IPUZ_STYLE_MARK_TL = 1 << 0,
  IPUZ_STYLE_MARK_T = 1 << 1,
  IPUZ_STYLE_MARK_TR = 1 << 2,
  IPUZ_STYLE_MARK_L = 1 << 3,
  IPUZ_STYLE_MARK_C = 1 << 4,
  IPUZ_STYLE_MARK_R = 1 << 5,
  IPUZ_STYLE_MARK_BL = 1 << 6,
  IPUZ_STYLE_MARK_B = 1 << 7,
  IPUZ_STYLE_MARK_BR = 1 << 8,
} IPuzStyleMark;

typedef enum
{
  IPUZ_STYLE_SIDES_TOP    = 1 << 0,
  IPUZ_STYLE_SIDES_RIGHT  = 1 << 1,
  IPUZ_STYLE_SIDES_BOTTOM = 1 << 2,
  IPUZ_STYLE_SIDES_LEFT   = 1 << 3,
} IPuzStyleSides;

#define IPUZ_STYLE_MARK_TOP (IPUZ_STYLE_MARK_TL | IPUZ_STYLE_MARK_T | IPUZ_STYLE_MARK_TR)
#define IPUZ_STYLE_MARK_CENTER_ROW (IPUZ_STYLE_MARK_L | IPUZ_STYLE_MARK_C | IPUZ_STYLE_MARK_R)
#define IPUZ_STYLE_MARK_BOTTOM (IPUZ_STYLE_MARK_BL | IPUZ_STYLE_MARK_B | IPUZ_STYLE_MARK_BR)
#define IPUZ_STYLE_MARK_LEFT (IPUZ_STYLE_MARK_TL | IPUZ_STYLE_MARK_L | IPUZ_STYLE_MARK_BL)
#define IPUZ_STYLE_MARK_CENTER_COL (IPUZ_STYLE_MARK_T | IPUZ_STYLE_MARK_C | IPUZ_STYLE_MARK_B)
#define IPUZ_STYLE_MARK_RIGHT (IPUZ_STYLE_MARK_TR | IPUZ_STYLE_MARK_R | IPUZ_STYLE_MARK_BR)

#define IPUZ_STYLE_SIDES_TOP_LEFT     (IPUZ_STYLE_SIDES_TOP|IPUZ_STYLE_SIDES_LEFT)
#define IPUZ_STYLE_SIDES_BOTTOM_RIGHT (IPUZ_STYLE_SIDES_BOTTOM|IPUZ_STYLE_SIDES_RIGHT)

#define IPUZ_STYLE_SIDES_HAS_LEFT(sides)   ((sides&IPUZ_STYLE_SIDES_LEFT)==IPUZ_STYLE_SIDES_LEFT)
#define IPUZ_STYLE_SIDES_HAS_RIGHT(sides)  ((sides&IPUZ_STYLE_SIDES_RIGHT)==IPUZ_STYLE_SIDES_RIGHT)
#define IPUZ_STYLE_SIDES_HAS_TOP(sides)    ((sides&IPUZ_STYLE_SIDES_TOP)==IPUZ_STYLE_SIDES_TOP)
#define IPUZ_STYLE_SIDES_HAS_BOTTOM(sides) ((sides&IPUZ_STYLE_SIDES_BOTTOM)==IPUZ_STYLE_SIDES_BOTTOM)

typedef void (*IPuzMarkFunc) (IPuzStyleMark  mark,
                              const gchar   *label,
                              gpointer       user_data);

typedef struct _IPuzStyle IPuzStyle;


GType             ipuz_style_get_type               (void) G_GNUC_CONST;
IPuzStyle        *ipuz_style_new                    (void);
IPuzStyle        *ipuz_style_new_from_json          (JsonNode         *node);
IPuzStyle        *ipuz_style_ref                    (IPuzStyle        *style);
void              ipuz_style_unref                  (IPuzStyle        *style);
gboolean          ipuz_style_equal                  (IPuzStyle        *a,
                                                     IPuzStyle        *b);
IPuzStyle        *ipuz_style_copy                   (IPuzStyle        *style);
void              ipuz_style_build                  (IPuzStyle        *style,
                                                     JsonBuilder      *builder);
gboolean          ipuz_style_is_empty               (IPuzStyle        *style);
const gchar      *ipuz_style_get_style_name         (IPuzStyle        *style);
void              ipuz_style_set_style_name         (IPuzStyle        *style,
                                                     const gchar      *style_name);
const gchar      *ipuz_style_get_named              (IPuzStyle        *style);
void              ipuz_style_set_named              (IPuzStyle        *style,
                                                     const gchar      *named);
gint              ipuz_style_get_border             (IPuzStyle        *style);
void              ipuz_style_set_border             (IPuzStyle        *style,
                                                     gint              border);
IPuzStyleShape    ipuz_style_get_shapebg            (IPuzStyle        *style);
void              ipuz_style_set_shapebg            (IPuzStyle        *style,
                                                     IPuzStyleShape    shapebg);
gboolean          ipuz_style_get_highlight          (IPuzStyle        *style);
void              ipuz_style_set_highlight          (IPuzStyle        *style,
                                                     gboolean          highlight);
IPuzStyleDivided  ipuz_style_get_divided            (IPuzStyle        *style);
void              ipuz_style_set_divided            (IPuzStyle        *style,
                                                     IPuzStyleDivided  divided);
void              ipuz_style_mark_foreach           (IPuzStyle        *style,
                                                     IPuzMarkFunc      func,
                                                     gpointer          user_data);
const gchar      *ipuz_style_get_label              (IPuzStyle        *style);
void              ipuz_style_set_label              (IPuzStyle        *style,
                                                     const gchar      *label);
const gchar      *ipuz_style_get_image_url          (IPuzStyle        *style);
void              ipuz_style_set_image_url          (IPuzStyle        *style,
                                                     const gchar      *image_url);
const gchar      *ipuz_style_get_imagebg_url        (IPuzStyle        *style);
void              ipuz_style_set_imagebg_url        (IPuzStyle        *style,
                                                     const gchar      *imagebg_url);
const gchar      *ipuz_style_get_bg_color           (IPuzStyle        *style);
void              ipuz_style_set_bg_color           (IPuzStyle        *style,
                                                     const gchar      *bg_color);
const gchar      *ipuz_style_get_text_color         (IPuzStyle        *style);
void              ipuz_style_set_text_color         (IPuzStyle        *style,
                                                     const gchar      *text_color);
const gchar      *ipuz_style_get_border_color       (IPuzStyle        *style);
void              ipuz_style_set_border_color       (IPuzStyle        *style,
                                                     const gchar      *border_color);
IPuzStyleSides    ipuz_style_get_barred             (IPuzStyle        *style);
void              ipuz_style_set_barred             (IPuzStyle        *style,
                                                     IPuzStyleSides    barred);
IPuzStyleSides    ipuz_style_get_dotted             (IPuzStyle        *style);
void              ipuz_style_set_dotted             (IPuzStyle        *style,
                                                     IPuzStyleSides    dotted);
IPuzStyleSides    ipuz_style_get_dashed             (IPuzStyle        *style);
void              ipuz_style_set_dashed             (IPuzStyle        *style,
                                                     IPuzStyleSides    dashed);
IPuzStyleSides    ipuz_style_get_lessthan           (IPuzStyle        *style);
void              ipuz_style_set_lessthan           (IPuzStyle        *style,
                                                     IPuzStyleSides    lessthan);
IPuzStyleSides    ipuz_style_get_greaterthan        (IPuzStyle        *style);
void              ipuz_style_set_greaterthan        (IPuzStyle        *style,
                                                     IPuzStyleSides    greaterthan);
IPuzStyleSides    ipuz_style_get_equal              (IPuzStyle        *style);
void              ipuz_style_set_equal              (IPuzStyle        *style,
                                                     IPuzStyleSides    equal);

IPuzStyleSides    ipuz_style_side_opposite          (IPuzStyleSides    side);
IPuzStyleSides    ipuz_style_sides_rotate_180       (IPuzStyleSides    sides);
IPuzStyleSides    ipuz_style_sides_rotate_rt        (IPuzStyleSides    sides);
IPuzStyleSides    ipuz_style_sides_rotate_lt        (IPuzStyleSides    sides);
IPuzStyleSides    ipuz_style_sides_flip_horiz       (IPuzStyleSides    sides);
IPuzStyleSides    ipuz_style_sides_flip_vert        (IPuzStyleSides    sides);

const gchar      *ipuz_style_shape_get_display_name (IPuzStyleShape    shapebg);



G_DEFINE_AUTOPTR_CLEANUP_FUNC(IPuzStyle, ipuz_style_unref)

G_END_DECLS

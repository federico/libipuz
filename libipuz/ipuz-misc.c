/* ipuz-misc.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#include <libipuz.h>

static void html_to_markup_start_element (GMarkupParseContext  *context,
                                          const gchar          *element_name,
                                          const gchar         **attribute_names,
                                          const gchar         **attribute_values,
                                          gpointer              user_data,
                                          GError              **error);
static void html_to_markup_end_element   (GMarkupParseContext  *context,
                                          const gchar          *element_name,
                                          gpointer              user_data,
                                          GError              **error);
static void html_to_markup_text          (GMarkupParseContext  *context,
                                          const gchar          *text,
                                          gsize                 text_len,
                                          gpointer              user_data,
                                          GError              **error);


static GMarkupParser parser =
{
  .start_element = html_to_markup_start_element,
  .end_element = html_to_markup_end_element,
  .text = html_to_markup_text,
};

/* We support the following elements (from the ipuz spec)
 * <b> ... </b> — Bold
 * <i> ... </i> — Italic
 * <s> ... </s> — Strikeout
 * <u> ... </u> — Underline
 * <em> ... </em> — Emphasis
 * <strong> ... </strong> — Stronger emphasis
 * <big> ... </big> — Bigger text
 * <small> ... </small> — Smaller text
 * <sup> ... </sup> — Superscript text
 * <sub> ... </sub> — Subscript text
 * <br> or <br/> — newline
 */
static void
html_to_markup_start_element (GMarkupParseContext  *context,
                              const gchar          *element_name,
                              const gchar         **attribute_names,
                              const gchar         **attribute_values,
                              gpointer              user_data,
                              GError              **error)
{
  GString *string = user_data;

  if (!g_strcmp0 (element_name, "clue"))
    return;
  /* Do we support pango-style attributes from outside? I think not,
   * but maybe there's a reason..? */
  else if (!g_strcmp0 (element_name, "span"))
    return;
  else if (!g_strcmp0 (element_name, "b"))
    g_string_append (string, "<b>");
  else if (!g_strcmp0 (element_name, "i")||
           !g_strcmp0 (element_name, "em"))
    g_string_append (string, "<i>");
  else if (!g_strcmp0 (element_name, "strong"))
    g_string_append (string, "<b><i>");
  else if (!g_strcmp0 (element_name, "s"))
    g_string_append (string, "<s>");
  else if (!g_strcmp0 (element_name, "big"))
    g_string_append (string, "<big>");
  else if (!g_strcmp0 (element_name, "small"))
    g_string_append (string, "<small>");
  else if (!g_strcmp0 (element_name, "sup"))
    g_string_append (string, "<sup>");
  else if (!g_strcmp0 (element_name, "sub"))
    g_string_append (string, "<sub>");
  else if (!g_strcmp0 (element_name, "br"))
    g_string_append (string, "\n");

  /* all other tags we just ignore. There's nothing much we can do
   * with them */
}

static void
html_to_markup_end_element (GMarkupParseContext  *context,
                            const gchar          *element_name,
                            gpointer              user_data,
                            GError              **error)
{
  GString *string = user_data;

  if (!g_strcmp0 (element_name, "clue"))
    return;
  else if (!g_strcmp0 (element_name, "span"))
    return;
  else if (!g_strcmp0 (element_name, "b"))
    g_string_append (string, "</b>");
  else if (!g_strcmp0 (element_name, "i")||
           !g_strcmp0 (element_name, "em"))
    g_string_append (string, "</i>");
  else if (!g_strcmp0 (element_name, "strong"))
    g_string_append (string, "</i></b>");
  else if (!g_strcmp0 (element_name, "s"))
    g_string_append (string, "</s>");
  else if (!g_strcmp0 (element_name, "big"))
    g_string_append (string, "</big>");
  else if (!g_strcmp0 (element_name, "small"))
    g_string_append (string, "</small>");
  else if (!g_strcmp0 (element_name, "sup"))
    g_string_append (string, "</sup>");
  else if (!g_strcmp0 (element_name, "sub"))
    g_string_append (string, "</sub>");
}

static void
html_to_markup_text (GMarkupParseContext  *context,
                     const gchar          *text,
                     gsize                 text_len,
                     gpointer              user_data,
                     GError              **error)
{
  g_autofree gchar *markup_text = NULL;
  GString *string = user_data;

  markup_text = g_markup_escape_text (text, text_len);
  g_string_append (string, markup_text);
}

gchar *
ipuz_html_to_markup (const gchar *src)
{
  g_autoptr (GMarkupParseContext) context = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *html_src = NULL;
  GString *string;

  /* short circuit empty strings */
  if (src == NULL || src[0] == '\0')
    return g_strdup (src);

  string = g_string_new (NULL);
  /* Most of the text we get isn't in xml tags, so we put it in a span
   * we can ignore. */
  html_src = g_strdup_printf ("<clue>%s</clue>", src);
  context = g_markup_parse_context_new (&parser, G_MARKUP_PREFIX_ERROR_POSITION, string, NULL);
  if (! g_markup_parse_context_parse (context, html_src, strlen (html_src), &error))
    goto error;
  if (! g_markup_parse_context_end_parse (context, &error))
    goto error;

  return g_string_free (string, FALSE);

 error:
  g_string_free (string, TRUE);
  return g_markup_escape_text (src, strlen (src));
}

GType
ipuz_puzzle_kind_to_gtype (IPuzPuzzleKind kind)
{
  switch (kind)
    {
    case IPUZ_PUZZLE_ACROSTIC:
      return IPUZ_TYPE_ACROSTIC;
    case IPUZ_PUZZLE_ARROWWORD:
      return IPUZ_TYPE_ARROWWORD;
    case IPUZ_PUZZLE_BARRED:
      return IPUZ_TYPE_BARRED;
    case IPUZ_PUZZLE_CROSSWORD:
      return IPUZ_TYPE_CROSSWORD;
    case IPUZ_PUZZLE_CRYPTIC:
      return IPUZ_TYPE_CRYPTIC;
    case IPUZ_PUZZLE_FILIPPINE:
      return IPUZ_TYPE_FILIPPINE;
    case IPUZ_PUZZLE_UNKNOWN:
    default:
      return G_TYPE_NONE;
    }
}

IPuzPuzzleKind
ipuz_puzzle_kind_from_gtype (GType gtype)
{
  if (gtype == IPUZ_TYPE_ACROSTIC)
    return IPUZ_PUZZLE_ACROSTIC;
  if (gtype == IPUZ_TYPE_ARROWWORD)
    return IPUZ_PUZZLE_ARROWWORD;
  if (gtype == IPUZ_TYPE_BARRED)
    return IPUZ_PUZZLE_BARRED;
  if (gtype == IPUZ_TYPE_CROSSWORD)
    return IPUZ_PUZZLE_CROSSWORD;
  if (gtype == IPUZ_TYPE_CRYPTIC)
    return IPUZ_PUZZLE_CRYPTIC;
  if (gtype == IPUZ_TYPE_FILIPPINE)
    return IPUZ_PUZZLE_FILIPPINE;

  return IPUZ_PUZZLE_UNKNOWN;
}

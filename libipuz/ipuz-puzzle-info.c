/* ipuz-puzzle-info.c
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include "ipuz-puzzle-info.h"
#include "ipuz-puzzle-info-private.h"


static void ipuz_puzzle_info_init       (IPuzPuzzleInfo      *self);
static void ipuz_puzzle_info_class_init (IPuzPuzzleInfoClass *klass);
static void ipuz_puzzle_info_finalize   (GObject             *object);


G_DEFINE_TYPE (IPuzPuzzleInfo, ipuz_puzzle_info, G_TYPE_OBJECT);


static void
ipuz_puzzle_info_init (IPuzPuzzleInfo *self)
{
  self->solution_chars = NULL;
  self->clue_lengths = NULL;
}

static void
ipuz_puzzle_info_class_init (IPuzPuzzleInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = ipuz_puzzle_info_finalize;
}

static void
ipuz_puzzle_info_finalize (GObject *object)
{
  IPuzPuzzleInfo *self;

  g_assert (IPUZ_IS_PUZZLE_INFO (object));

  self = IPUZ_PUZZLE_INFO (object);

  g_clear_pointer (&self->charset, ipuz_charset_unref);
  g_clear_pointer (&self->solution_chars, ipuz_charset_unref);
  g_clear_pointer (&self->clue_lengths, ipuz_charset_unref);
}

/* Public Methods */
IPuzPuzzleFlags
ipuz_puzzle_info_get_flags (IPuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), 0);

  return self->flags;
}

IPuzCellStats
ipuz_puzzle_info_get_cell_stats (IPuzPuzzleInfo *self)
{
  IPuzCellStats stats = {0, };

  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), stats);

  return self->cell_stats;
}

IPuzCharset *
ipuz_puzzle_info_get_charset (IPuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), NULL);

  return self->charset;
}

IPuzCharset *
ipuz_puzzle_info_get_solution_chars (IPuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), NULL);

  return self->solution_chars;
}

IPuzCharset *
ipuz_puzzle_info_get_clue_lengths (IPuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), NULL);

  return self->clue_lengths;
}

guint
ipuz_puzzle_info_get_pangram_count (IPuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), 0);

  return self->pangram_count;
}

GArray *
ipuz_puzzle_info_get_unches (IPuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), NULL);

  return self->unche_list;
}

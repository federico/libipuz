/* ipuz-puzzle.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-style.h>
#include <libipuz/ipuz-puzzle-info.h>


G_BEGIN_DECLS


typedef enum
{
  IPUZ_PUZZLE_ACROSTIC,
  IPUZ_PUZZLE_ARROWWORD,
  IPUZ_PUZZLE_BARRED,
  IPUZ_PUZZLE_CROSSWORD,
  IPUZ_PUZZLE_CRYPTIC,
  IPUZ_PUZZLE_FILIPPINE,
  IPUZ_PUZZLE_UNKNOWN,
} IPuzPuzzleKind;


#define IPUZ_TYPE_PUZZLE (ipuz_puzzle_get_type ())
G_DECLARE_DERIVABLE_TYPE (IPuzPuzzle, ipuz_puzzle, IPUZ, PUZZLE, GObject);


typedef void (*IPuzStyleForeachFunc) (const gchar *style_name,
                                      IPuzStyle   *style,
                                      gpointer     user_data);


typedef struct _IPuzPuzzleClass IPuzPuzzleClass;
struct _IPuzPuzzleClass
{
  GObjectClass parent_class;

  /* Virtual functions */
  void                (*load_node)      (IPuzPuzzle     *puzzle,
                                         const char     *member_name,
                                         JsonNode       *node);
  void                (*post_load_node) (IPuzPuzzle     *puzzle,
                                         const char     *member_name,
                                         JsonNode       *node);
  void                (*fixup)          (IPuzPuzzle     *puzzle);
  void                (*validate)       (IPuzPuzzle     *puzzle);
  gboolean            (*equal)          (IPuzPuzzle     *puzzle_a,
                                         IPuzPuzzle     *puzzle_b);
  void                (*build)          (IPuzPuzzle     *puzzle,
                                         JsonBuilder    *builder);
  IPuzPuzzleFlags     (*get_flags)      (IPuzPuzzle     *puzzle);
  void                (*clone)          (IPuzPuzzle     *src,
                                         IPuzPuzzle     *dest);
  const char * const *(*get_kind_str)   (IPuzPuzzle     *puzzle);
  void                (*set_style)      (IPuzPuzzle     *puzzle,
                                         const char     *style_name,
                                         IPuzStyle      *style);
  void                (*calculate_info) (IPuzPuzzle     *puzzle,
                                         IPuzPuzzleInfo *info);
};


IPuzPuzzle      *ipuz_puzzle_new_from_json   (JsonNode              *root,
                                              GError               **error);
IPuzPuzzle      *ipuz_puzzle_new_from_file   (const char            *filename,
                                              GError               **error);
IPuzPuzzle      *ipuz_puzzle_new_from_stream (GInputStream          *stream,
                                              GCancellable          *cancellable,
                                              GError               **error);
IPuzPuzzle      *ipuz_puzzle_new_from_data   (const gchar           *data,
                                              gsize                  length,
                                              GError               **error);
gboolean         ipuz_puzzle_save_to_file    (IPuzPuzzle            *puzzle,
                                              const char            *filename,
                                              GError               **error);
gboolean         ipuz_puzzle_save_to_stream  (IPuzPuzzle            *puzzle,
                                              GOutputStream         *stream,
                                              GCancellable          *cancellable,
                                              GError               **error);
gchar           *ipuz_puzzle_save_to_data    (IPuzPuzzle            *puzzle,
                                              gsize                 *length);
IPuzPuzzle      *ipuz_puzzle_deep_copy       (IPuzPuzzle            *puzzle);

IPuzPuzzleKind   ipuz_puzzle_get_puzzle_kind (IPuzPuzzle            *puzzle);
IPuzPuzzleFlags  ipuz_puzzle_get_flags       (IPuzPuzzle            *puzzle);
IPuzPuzzleInfo  *ipuz_puzzle_get_info        (IPuzPuzzle            *puzzle);
gboolean         ipuz_puzzle_equal           (IPuzPuzzle            *puzzle_a,
                                              IPuzPuzzle            *puzzle_b);
IPuzCharset     *ipuz_puzzle_get_charset     (IPuzPuzzle            *self);
IPuzStyle       *ipuz_puzzle_get_style       (IPuzPuzzle            *self,
                                              const gchar           *style_name);
void             ipuz_puzzle_set_style       (IPuzPuzzle            *self,
                                              const gchar           *style_name,
                                              IPuzStyle             *style);
void             ipuz_puzzle_style_foreach   (IPuzPuzzle            *self,
                                              IPuzStyleForeachFunc   func,
                                              gpointer               user_data);


G_END_DECLS

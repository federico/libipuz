/* ipuz-acrostic.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include <libipuz/ipuz-acrostic.h>
#include "ipuz-magic.h"

typedef struct _IPuzAcrosticPrivate
{
  IPuzClue *quote_clue;
} IPuzAcrosticPrivate;


static void                ipuz_acrostic_init         (IPuzAcrostic      *self);
static void                ipuz_acrostic_class_init   (IPuzAcrosticClass *klass);
static void                ipuz_acrostic_finalize     (GObject           *object);
static void                ipuz_acrostic_clone        (IPuzPuzzle        *src,
		                                       IPuzPuzzle        *dest);
static gboolean            ipuz_acrostic_equal        (IPuzPuzzle        *puzzle_a,
                                                       IPuzPuzzle        *puzzle_b);
static void                ipuz_acrostic_fixup        (IPuzPuzzle        *puzzle);
static const gchar *const *ipuz_acrostic_get_kind_str (IPuzPuzzle        *puzzle);


G_DEFINE_TYPE_WITH_CODE (IPuzAcrostic, ipuz_acrostic, IPUZ_TYPE_CROSSWORD,
		         G_ADD_PRIVATE (IPuzAcrostic));

static void
ipuz_acrostic_init (IPuzAcrostic *self)
{
  /* Pass */
}

static void
ipuz_acrostic_class_init (IPuzAcrosticClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  IPuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);

  object_class->finalize = ipuz_acrostic_finalize;
  puzzle_class->clone = ipuz_acrostic_clone;
  puzzle_class->equal = ipuz_acrostic_equal;
  puzzle_class->fixup = ipuz_acrostic_fixup;
  puzzle_class->get_kind_str = ipuz_acrostic_get_kind_str;
}

static void
ipuz_acrostic_finalize (GObject *object)
{
  IPuzAcrosticPrivate *priv;

  g_return_if_fail (object != NULL);

  priv = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (object));

  ipuz_clue_free (priv->quote_clue);

  G_OBJECT_CLASS (ipuz_acrostic_parent_class)->finalize (object);
}

static void
ipuz_acrostic_clone (IPuzPuzzle *src,
		     IPuzPuzzle *dest)
{
  IPuzAcrosticPrivate *src_priv, *dest_priv;

  g_assert (src != NULL);
  g_assert (dest != NULL);

  src_priv = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (src));
  dest_priv = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (dest));

  IPUZ_PUZZLE_CLASS (ipuz_acrostic_parent_class)->clone (src, dest);

  dest_priv->quote_clue = ipuz_clue_copy (src_priv->quote_clue);
}

static IPuzClue *
calculate_quote_clue (IPuzAcrostic *self)
{
  IPuzCrossword *xword = IPUZ_CROSSWORD (self);
  IPuzClue *quote_clue = ipuz_clue_new ();

  guint rows = ipuz_crossword_get_height (xword);
  guint columns = ipuz_crossword_get_width (xword);
  guint row, column;

  for (row = 0; row < rows; row++)
    {
      for (column = 0; column < columns; column++)
        {
	  IPuzCell *cell;
	  IPuzCellCoord coord = {
	    .row = row,
	    .column = column,
	  };

	  cell = ipuz_crossword_get_cell (xword, coord);

	  if (IPUZ_CELL_IS_GUESSABLE (cell))
	    {
	      g_array_append_val (quote_clue->cells, coord);
	    }
	}
    }
  return quote_clue;
}

static IPuzClue *
extract_quote_clue (IPuzAcrostic *self)
{
  for (guint n = 0; n < ipuz_crossword_get_n_clue_sets (IPUZ_CROSSWORD (self)); n++)
    {
      GArray *clues;

      clues = ipuz_crossword_get_clues (IPUZ_CROSSWORD (self),
                                        ipuz_crossword_clue_set_get_dir (IPUZ_CROSSWORD (self), n));
      g_assert (clues);
      for (guint i = 0; i < clues->len; i++)
        {
          IPuzClue *clue = g_array_index (clues, IPuzClue *, i);
          if (g_strcmp0 (ipuz_clue_get_clue_text (clue), _IPUZ_ACROSTIC_QUOTE_STR) == 0)
            {
              IPuzClue *quote_clue;
              quote_clue = ipuz_clue_copy (clue);
              ipuz_crossword_unlink_clue (IPUZ_CROSSWORD (self), clue);

              ipuz_clue_set_direction (quote_clue, IPUZ_CLUE_DIRECTION_NONE);
              ipuz_clue_set_clue_text (quote_clue, NULL);
              return quote_clue;
            }
        }
    }

  return NULL;
}

static void
fix_quote_clue (IPuzAcrostic *self)
{
  IPuzAcrosticPrivate *priv;

  priv = ipuz_acrostic_get_instance_private (self);

  priv->quote_clue = extract_quote_clue (self);

  if (priv->quote_clue == NULL)
    priv->quote_clue = calculate_quote_clue (self);
}

static gboolean
ipuz_acrostic_equal (IPuzPuzzle *puzzle_a,
                     IPuzPuzzle *puzzle_b)
{
  IPuzAcrosticPrivate *priv_a, *priv_b;

  g_return_val_if_fail (IPUZ_IS_ACROSTIC (puzzle_b), FALSE);

  priv_a = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (puzzle_a));
  priv_b = ipuz_acrostic_get_instance_private (IPUZ_ACROSTIC (puzzle_b));

  if (! ipuz_clue_equal (priv_a->quote_clue, priv_b->quote_clue))
    return FALSE;

  return IPUZ_PUZZLE_CLASS (ipuz_acrostic_parent_class)->equal (puzzle_a,
                                                                puzzle_b);
}

static void
ipuz_acrostic_fixup (IPuzPuzzle *puzzle)
{
  IPUZ_PUZZLE_CLASS (ipuz_acrostic_parent_class) -> fixup (puzzle);

  fix_quote_clue (IPUZ_ACROSTIC (puzzle));
}

static const gchar *const *
ipuz_acrostic_get_kind_str (IPuzPuzzle *puzzle)
{
  static const char *kind_str[] =
    {
      "http://ipuz.org/acrostic#1",
      NULL
    };

  return kind_str;
}

/*
 * Public Methods
 */

IPuzPuzzle *
ipuz_acrostic_new (void)
{
  IPuzPuzzle *acrostic;

  acrostic = g_object_new (IPUZ_TYPE_ACROSTIC,
			   NULL);

  return acrostic;
}

IPuzClue *
ipuz_acrostic_get_quote_clue (IPuzAcrostic *self)
{
  IPuzAcrosticPrivate *priv;

  priv = ipuz_acrostic_get_instance_private (self);

  return priv->quote_clue;
}
